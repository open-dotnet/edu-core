﻿using EduCore.Common.DB;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace EduCore.Extensions
{
    /// <summary>
    /// SqlSugar 启动服务
    /// </summary>
    public static class SqlsugarSetup
    {
        public static void AddSqlsugarSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddScoped<SqlSugar.ISqlSugarClient>(o =>
            {
                return new SqlSugar.SqlSugarClient(new SqlSugar.ConnectionConfig()
                {
                    ConnectionString = BaseDBConfig.ConnectionString,
                    DbType = (SqlSugar.DbType)BaseDBConfig.DbType,
                    IsAutoCloseConnection = true,
                    InitKeyType = SqlSugar.InitKeyType.SystemTable
                });
            });
        }
    }
}
