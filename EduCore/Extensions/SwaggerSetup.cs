﻿using EduCore.Filter;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.IO;

namespace EduCore.Extensions
{
  /// <summary>
  /// Swagger 启动服务
  /// </summary>
  public static class SwaggerSetup
  {

    private static readonly ILog log =
    LogManager.GetLogger(typeof(GlobalExceptionsFilter));

    public static void AddSwaggerSetup(this IServiceCollection services)
    {
      if (services == null) throw new ArgumentNullException(nameof(services));

      var basePath = AppContext.BaseDirectory;
      services.AddSwaggerGen(c =>
      {
        //遍历出全部的版本，做文档信息展示
        {
          c.SwaggerDoc("v1", new OpenApiInfo
          {
            Version = null,
            Title = "API文档",
            Description = "API及Model文档",
            TermsOfService = null,
          });
          c.OrderActionsBy(o => o.RelativePath);
        }

        try
        {
          //就是这里
          var xmlPath = Path.Combine(basePath, "EduCore.xml");//这个就是刚刚配置的xml文件名
          c.IncludeXmlComments(xmlPath, true);//默认的第二个参数是false，这个是controller的注释，记得修改

          var xmlModelPath = Path.Combine(basePath, "EduCore.Models.xml");//这个就是Model层的xml文件名
          c.IncludeXmlComments(xmlModelPath);
        }
        catch (Exception ex)
        {
          log.Error("获取XML信息失败请检查并拷贝。\n" + ex.Message);
        }

        c.OperationFilter<AddResponseHeadersFilter>();
        c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
        c.OperationFilter<SecurityRequirementsOperationFilter>();
      });
    }
  }
}
