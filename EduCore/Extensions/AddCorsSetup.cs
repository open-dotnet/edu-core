﻿using EduCore.Common;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EduCore.Extensions
{
  /// <summary>
  /// Cors 启动服务
  /// </summary>
  public static class CorsSetup
  {
    public static void AddCorsSetup(this IServiceCollection services)
    {
      if (services == null) throw new ArgumentNullException(nameof(services));

      services.AddCors(c =>
      {
        c.AddPolicy("LimitRequests", policy =>
              {
            policy
                  .WithOrigins(Appsettings.app(new string[] { "Startup", "Cors", "IPs" }).Split(','))
                  .AllowAnyHeader()//Ensures that the policy allows any header.
                  .AllowAnyMethod();
          });
      });
    }
  }
}