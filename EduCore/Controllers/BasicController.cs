﻿using EduCore.Common.Authorization;
using EduCore.Common.Helper;
using EduCore.IServices;
using EduCore.Models;
using EduCore.Models.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using EduCore.Models.ViewModel;

namespace EduCore.Controllers
{
    /// <summary>
    /// 登录管理【无权限】
    /// </summary>
    [Produces("application/json")]
    [Route("api/basic")]
    public class BasicController : Controller
    {

        readonly IServiceGrade _serviceGrade;
        readonly IServiceSubject _serviceSubject;

        public BasicController(IServiceGrade serviceGrade, IServiceSubject serviceSubject)
        {
            _serviceGrade = serviceGrade;
            _serviceSubject = serviceSubject;
        }

        #region 学科表
        /// <summary>
        /// 学科列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("subject")]
        [Authorize(Policy = "AdminOrUser")]
        public async Task<Message<object>> SubjectList(int page, int pagesize = 10)
        {
            var message = new Message<object>();
            var result = await _serviceSubject.QueryPage(null, page, pagesize = 10, "sort");
            message.code = 20000;
            message.msg = "ok";
            message.data = result;
            return message;
        }


        /// <summary>
        /// 添加学科
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("addsubject")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> AddSubject([FromBody] Subject item)
        {
            var message = new Message<string>();
            try
            {
                var exist = await _serviceSubject.ExistAsync(item);
                if (exist)
                {
                    message.code = 20003;
                    message.msg = "信息已存在，请修改后重试!";
                }
                else
                {
                    var data = await _serviceSubject.Add(item);
                    if (data > 0)
                    {

                        message.code = 20002;
                        message.msg = "添加成功！";
                    }
                    else
                    {
                        message.code = 20003;
                        message.msg = "添加失败!";
                    }
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }

        /// <summary>
        /// 更新学科
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatesubject")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> UpdateSubject([FromBody] Subject item)
        {
            var message = new Message<string>();
            try
            {
                var exist = await _serviceSubject.ExistAsync(item, true);
                if (exist)
                {
                    message.code = 20003;
                    message.msg = "名称已存在，请修改后重试!";
                }
                else
                {
                    var data = await _serviceSubject.Update(item);
                    if (data)
                    {

                        message.code = 20002;
                        message.msg = "更新成功！";
                    }
                    else
                    {
                        message.code = 20003;
                        message.msg = "更新失败!";
                    }
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
        /// <summary>
        /// 删除学科
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delsubject")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> DelSubject(int id)
        {
            var message = new Message<string>();
            try
            {
                var data = await _serviceSubject.DeleteById(id);
                if (data)
                {
                    message.code = 20002;
                    message.msg = "删除成功！";
                }
                else
                {
                    message.code = 20003;
                    message.msg = "删除失败!";
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
        #endregion



        /// <summary>
        /// 学科列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("grade")]
        [Authorize(Policy = "AdminOrUser")]
        public async Task<Message<object>> GradeList(int page, int pagesize = 10)
        {
            var message = new Message<object>();
            var result = await _serviceGrade.QueryPage(null, page, pagesize = 10, "sort");
            message.code = 20000;
            message.msg = "ok";
            message.data = result;
            return message;
        }


        /// <summary>
        /// 添加年级
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("addgrade")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> AddGrade([FromBody] Grade item)
        {
            var message = new Message<string>();
            try
            {
                var exist = await _serviceGrade.ExistAsync(item);
                if (exist)
                {
                    message.code = 20003;
                    message.msg = "信息已存在，请修改后重试!";
                }
                else
                {
                    var data = await _serviceGrade.Add(item);
                    if (data > 0)
                    {

                        message.code = 20002;
                        message.msg = "添加成功！";
                    }
                    else
                    {
                        message.code = 20003;
                        message.msg = "添加失败!";
                    }
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }

        /// <summary>
        /// 更新年级
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updategrade")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> UpdateGrade([FromBody] Grade item)
        {
            var message = new Message<string>();
            try
            {
                var exist = await _serviceGrade.ExistAsync(item, true);
                if (exist)
                {
                    message.code = 20003;
                    message.msg = "名称已存在，请修改后重试!";
                }
                else
                {
                    var data = await _serviceGrade.Update(item);
                    if (data)
                    {

                        message.code = 20002;
                        message.msg = "更新成功！";
                    }
                    else
                    {
                        message.code = 20003;
                        message.msg = "更新失败!";
                    }
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
        /// <summary>
        /// 删除年级
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delgrade")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> DelGrade(int id)
        {
            var message = new Message<string>();
            try
            {
                var data = await _serviceGrade.DeleteById(id);
                if (data)
                {
                    message.code = 20002;
                    message.msg = "删除成功！";
                }
                else
                {
                    message.code = 20003;
                    message.msg = "删除失败!";
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }

    }
}