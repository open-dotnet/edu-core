﻿using EduCore.Common.Authorization;
using EduCore.Common.Helper;
using EduCore.IServices;
using EduCore.Models;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Senparc.Weixin;
using Senparc.Weixin.Exceptions;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EduCore.Controllers
{
    /// <summary>
    /// 登录管理
    /// </summary>
    [Produces("application/json")]
    [Route("api/login")]
    [AllowAnonymous]
    public class LoginController : Controller
    {
        readonly IServiceUser _serviceUser;
        readonly IServiceWechatAuthUser _serviceWechatAuthUser;
        private string appId = Config.SenparcWeixinSetting.WeixinAppId;
        private string appSecret = Config.SenparcWeixinSetting.WeixinAppSecret;

        public LoginController(IServiceUser serviceUser, IServiceWechatAuthUser serviceWechatAuthUser)
        {
            _serviceUser = serviceUser;
            _serviceWechatAuthUser = serviceWechatAuthUser;
        }


        /// <summary>
        /// 普通用户登录
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("user")]
        public async Task<object> UserLoginAsync([FromBody]UserLoginView user)
        {
            var message = new Message<object>();
            user.password = MD5Helper.MD5Encrypt64(user.password);
            var result = (await _serviceUser.Query(u => u.UserName == user.username
            && u.Password == user.password)).FirstOrDefault();
            if (result != null)
            {
                if (result.IsDeleted)
                {
                    message.code = 20006;
                    message.msg = "当前用户已被禁用，请联系管理员！";
                }
                else
                {
                    TokenModelJwt tokenModel = new TokenModelJwt
                    {
                        Uid = result.Id,
                        Role = Enum.GetName(typeof(EnumHelper.Roles), result.Role)
                    };
                    var jwtStr = JwtHelper.IssueJwt(tokenModel);
                    message.code = 20000;
                    message.msg = "ok";
                    message.data = jwtStr;
                }

            }
            else
            {
                message.code = 20001;
                message.msg = "用户名或密码错误";
            }
            return message;
        }


        /// <summary>
        /// 微信用户登录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("wechattest")]
        public object WechatTestLogin()
        {
            var message = new Message<object>();
            TokenModelWechatJwt tokenModel = new TokenModelWechatJwt
            {
                Uid = "oxg-TwhMO2H49BATOG1JEdMVBAds",
                Role = Enum.GetName(typeof(EnumHelper.Roles), EnumHelper.Roles.wechat)
            };
            string jwtStr = JwtHelper.IssueWechatJwt(tokenModel);
            message.code = 20000;
            message.msg = "ok";
            message.data = jwtStr;
            return message;
        }


        /// <summary>
        /// OAuthScope.snsapi_userinfo方式回调
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("wechat")]
        public async Task<object> WechatLogin(string code, string state)
        {
            var message = new Message<object>();
            if (code == "admin" && state == "admin")
            {
                TokenModelWechatJwt tokenModel = new TokenModelWechatJwt
                {
                    Uid = "admin",
                    Role = Enum.GetName(typeof(EnumHelper.Roles), EnumHelper.Roles.wechat)
                };
                string jwtStr = JwtHelper.IssueWechatJwt(tokenModel);
                message.code = 20000;
                message.msg = "ok";
                message.data = jwtStr;
                return message;
            }
            if (string.IsNullOrEmpty(code))
            {
                message.code = 20003;
                message.msg = "您拒绝了授权！";
                return message;
            }
            if (state != "HongDa")
            {
                message.code = 20003;
                message.msg = "授权码不正确！";
                return message;
            }

            OAuthAccessTokenResult result;
            try
            {
                result = OAuthApi.GetAccessToken(appId, appSecret, code);
            }
            catch (Exception ex)
            {
                message.code = 20003;
                message.msg = ex.Message;
                return message;
            }
            if (result.errcode != ReturnCode.请求成功)
            {
                message.code = 20003;
                message.msg = result.errmsg;
                return message;
            }

            try
            {
                OAuthUserInfo userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
                var exsit = await _serviceWechatAuthUser.ExistAsync(userInfo.openid);
                if (exsit)
                {
                    var wechatuser = new WechatAuthUser();
                    wechatuser.Openid = userInfo.openid;
                    wechatuser.Nickname = userInfo.nickname;
                    wechatuser.Sex = userInfo.sex;
                    wechatuser.Headimgurl = userInfo.headimgurl;
                    wechatuser.Province = userInfo.province;
                    wechatuser.City = userInfo.city;
                    wechatuser.Country = userInfo.country;
                    wechatuser.UpdateTime = DateTime.Now;
                    await _serviceWechatAuthUser.Update(wechatuser, null, new List<string> { "CreateTime" });
                }
                else
                {
                    var wechatuser = new WechatAuthUser();
                    wechatuser.Openid = userInfo.openid;
                    wechatuser.Nickname = userInfo.nickname;
                    wechatuser.Sex = userInfo.sex;
                    wechatuser.Headimgurl = userInfo.headimgurl;
                    wechatuser.Province = userInfo.province;
                    wechatuser.City = userInfo.city;
                    wechatuser.Country = userInfo.country;
                    wechatuser.CreateTime = DateTime.Now;
                    wechatuser.UpdateTime = DateTime.Now;
                    await _serviceWechatAuthUser.Add(wechatuser);
                }

                TokenModelWechatJwt tokenModel = new TokenModelWechatJwt
                {
                    Uid = userInfo.openid,
                    Role = Enum.GetName(typeof(EnumHelper.Roles), EnumHelper.Roles.wechat)
                };
                string jwtStr = JwtHelper.IssueWechatJwt(tokenModel);
                message.code = 20000;
                message.msg = "ok";
                message.data = jwtStr;
                return message;
            }
            catch (ErrorJsonResultException ex)
            {
                message.code = 20003;
                message.msg = ex.Message;
                return message;
            }
        }

        /// <summary>
        /// 测试 MD5 加密字符串
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Md5Password")]
        public string Md5Password(string password = "")
        {
            return MD5Helper.MD5Encrypt64(password);
        }
    }
}