﻿using EduCore.IServices;
using EduCore.Models;
using EduCore.Models.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EduCore.Controllers
{
    /// <summary>
    /// 微信用户信息
    /// </summary>
    [Produces("application/json")]
    [Route("api/wechat")]
    public class WechatController : Controller
    {

        readonly IServiceWechatAuthUser _serviceWechatAuthUser;

        public WechatController(IServiceWechatAuthUser serviceWechatAuthUser)
        {
            _serviceWechatAuthUser = serviceWechatAuthUser;
        }

        /// <summary>
        /// 微信用户列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("list")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<object>> WechatList(string name, int page, int pagesize = 10)
        {
            var message = new Message<object>();
            PageModel<WechatAuthUser> result = null;
            try
            {
                if (string.IsNullOrEmpty(name))
                {
                    result = await _serviceWechatAuthUser.QueryPage(null, page, pagesize, "updatetime desc");
                }
                else
                {
                    result = await _serviceWechatAuthUser.QueryPage(w => w.Nickname.Contains(name), page, pagesize, "updatetime desc");
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                message.code = 20000;
                message.msg = "ok";
                message.data = result;
            }
            return message;
        }
    }
}