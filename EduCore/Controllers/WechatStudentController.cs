﻿using EduCore.IServices;
using EduCore.Models;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EduCore.Controllers
{

    /// <summary>
    /// 微信用户
    /// </summary>
    [Produces("application/json")]
    [Route("api/wechatstu")]
    public class WechatStudentController : Controller
    {

        readonly IServiceWechatStudent _serviceWechatStudent;

        public WechatStudentController(IServiceWechatStudent serviceWechatStudent)
        {
            _serviceWechatStudent = serviceWechatStudent;
        }

        /// <summary>
        /// 微信绑定列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("list")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<object>> WechatStudentList(string nickname = null, string stuname = null, int page = 1, int pagesize = 10)
        {
            var message = new Message<object>();
            PageModel<WechatStudentInfoView> result = null;
            try
            {
                result = await _serviceWechatStudent.WechatStudentInfoList(nickname, stuname, page, pagesize);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                message.code = 20000;
                message.msg = "ok";
                message.data = result;
            }
            return message;
        }


        /// <summary>
        /// 微信绑定学生信息
        /// </summary>
        /// <param name="wechatstu"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("add")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> AddStuWechat([FromBody]WechatStudent wechatstu)
        {
            var message = new Message<string>();
            try
            {
                var exsit = await _serviceWechatStudent.ExistAsync(wechatstu.Openid, wechatstu.StuId);
                if (exsit)
                {
                    message.code = 20003;
                    message.msg = "当前学生已绑定到该微信";
                }
                else
                {
                    var resutl = await _serviceWechatStudent.Add(wechatstu);
                    message.code = 20000;
                    message.msg = "绑定微信成功";
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
        /// <summary>
        /// 解绑微信学生
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delete")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> DelStuWechat(string id)
        {
            var message = new Message<string>();
            try
            {
                var result = await _serviceWechatStudent.DeleteById(id);
                if (result)
                {
                    message.code = 20002;
                    message.msg = "解绑成功";
                }
                else
                {
                    message.code = 20003;
                    message.msg = "解绑失败，请刷新后再操作";
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
    }
}