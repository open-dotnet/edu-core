﻿using Microsoft.AspNetCore.Mvc;

namespace EduCore.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index(string errmsg)
        {
            ViewBag.ErrMsg = errmsg;
            return View();
        }
    }
}