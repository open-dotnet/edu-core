﻿using EduCore.Common;
using EduCore.IServices;
using Microsoft.AspNetCore.Mvc;
using Senparc.Weixin;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.Entities.Request;
using System;

namespace EduCore.Controllers
{
  /// <summary>
  /// 微信用户
  /// </summary>
  [Produces("application/json")]
  [Route("wechat")]
  public class WechatMessageController : Controller
  {
    public static readonly string Token = Config.SenparcWeixinSetting.Token;//与微信公众账号后台的Token设置保持一致，区分大小写。
    public static readonly string EncodingAESKey = Config.SenparcWeixinSetting.EncodingAESKey;//与微信公众账号后台的EncodingAESKey设置保持一致，区分大小写。
    public static readonly string AppId = Config.SenparcWeixinSetting.WeixinAppId;//与微信公众账号后台的AppId设置保持一致，区分大小写。
    readonly Func<string> _getRandomFileName = () => SystemTime.Now.ToString("yyyyMMdd-HHmmss") + Guid.NewGuid().ToString("n").Substring(0, 6);
    readonly IServiceWechatAuthUser _serviceWechatAuthUser;
    readonly IAppLogService _appLogService;

    public WechatMessageController(IServiceWechatAuthUser serviceWechatAuthUser, IAppLogService appLogService)
    {
      _appLogService = appLogService;
      _serviceWechatAuthUser = serviceWechatAuthUser;
    }
    /// <summary>
    /// 微信后台验证地址
    /// </summary>
    [HttpGet]
    [ActionName("Index")]
    public ActionResult Get(PostModel postModel, string echostr)
    {
      if (CheckSignature.Check(postModel.Signature, postModel.Timestamp, postModel.Nonce, Token))
      {
        return Content(echostr); //返回随机字符串则表示验证通过
      }
      else
      {
        return Content("failed:" + postModel.Signature + "," + CheckSignature.GetSignature(postModel.Timestamp, postModel.Nonce, Token) + "。" +
            "如果你在浏览器中看到这句话，说明此地址可以被作为微信公众账号后台的Url，请注意保持Token一致。");
      }
    }
    /// <summary>
    /// 用户消息
    /// </summary>
    [HttpPost]
    [ActionName("Index")]
    public ActionResult Post(PostModel postModel)
    {
      return Content("");
    }

  }
}