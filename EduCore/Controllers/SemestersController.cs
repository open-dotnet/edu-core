﻿using EduCore.Common.Authorization;
using EduCore.Common.Helper;
using EduCore.IServices;
using EduCore.Models;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EduCore.Controllers
{
    /// <summary>
    /// 登录管理【无权限】
    /// </summary>
    [Produces("application/json")]
    [Route("api/semesters")]
    public class SemestersController : Controller
    {

        readonly IServiceSemesters _serviceSemesters;
        readonly IServiceStudent _serviceStudent;
        readonly IServiceStuSemInfo _serviceStuSemInfo;
        readonly IServiceStuSemRecord _serviceStuSemRecord;

        public SemestersController(IServiceSemesters serviceSemesters,
                                   IServiceStudent serviceStudent,
                                   IServiceStuSemRecord serviceStuSemRecord,
        IServiceStuSemInfo serviceStuSemInfo)
        {
            _serviceSemesters = serviceSemesters;
            _serviceStudent = serviceStudent;
            _serviceStuSemInfo = serviceStuSemInfo;
            _serviceStuSemRecord = serviceStuSemRecord;
        }

        #region 学期信息管理
        /// <summary>
        /// 获取学期列表
        /// </summary>
        /// <param name="isDeleted"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("semesterslist")]
        [Authorize(Policy = "AdminOrUser")]
        public async Task<object> SemestersListAsync(bool? isDeleted, int page, int pagesize = 10)
        {
            var message = new Message<object>();
            try
            {
                if (isDeleted == null)
                {
                    var data = await _serviceSemesters.QueryPage(null, page, pagesize, "IsDeleted, Id desc ");
                    message.data = data;
                }
                else
                {
                    var data = await _serviceSemesters.QueryPage((s => s.IsDeleted == isDeleted), page, pagesize, " Id desc ");
                    message.data = data;
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }

        /// <summary>
        /// 修改学期信息
        /// </summary>
        /// <param name="semesters"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatesemesters")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> UpdateSemesters([FromBody] Semesters semesters)
        {
            var message = new Message<string>();
            try
            {
                var data = await _serviceSemesters.Update(semesters, new List<string> { "ClassSize", "Remark" }, null);
                if (data)
                {
                    message.code = 20002;
                    message.msg = "修改成功！";
                }
                else
                {
                    message.code = 20003;
                    message.msg = "修改失败!";
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
        /// <summary>
        /// 添加学期信息
        /// </summary>
        /// <param name="semesters"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("addsemesters")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> AddSemesterst([FromBody] Semesters semesters)
        {
            var message = new Message<string>();

            try
            {
                var exist = await _serviceSemesters.ExistAsync(semesters);
                if (exist)
                {
                    message.code = 20003;
                    message.msg = "学期已存在，请修改后再提交！";
                }
                else
                {
                    var data = await _serviceSemesters.Add(semesters);
                    if (data > 0)
                    {

                        message.code = 20002;
                        message.msg = "添加成功！";
                    }
                    else
                    {
                        message.code = 20003;
                        message.msg = "添加失败!";
                    }
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
        /// <summary>
        /// 删除学期信息
        /// </summary>
        /// <param name="id">学期表Id</param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delsemesters")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> DelSemesterst(int id, bool isDeleted)
        {
            var data = await _serviceSemesters.Update(new Semesters() { Id = id, IsDeleted = isDeleted }, new List<string> { "IsDeleted" });
            if (data)
            {
                return new Message<string>()
                {
                    code = 20002,
                    msg = "删除成功！"
                };
            }
            else
            {
                return new Message<string>()
                {
                    code = 20003,
                    msg = "删除失败!"
                };
            }
        }

        #endregion

        #region 报课信息
        /// <summary>
        /// 获取学籍列表
        /// </summary>
        /// <param name="stuName"></param>
        /// <param name="stuNum"></param>
        /// <param name="semId"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("stuseminfolist")]
        [Authorize(Policy = "AdminOrUser")]
        public async Task<object> StuSemInfoListAsync(string stuName, string stuNum, int semId, int page, int pagesize = 10)
        {
            var message = new Message<object>();
            try
            {
                var data = await _serviceStuSemInfo.StuSemInfoViewList(stuName, stuNum, semId, page, pagesize);
                message.data = data;

            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }


        /// <summary>
        /// 获取当前微信单个学生学籍列表
        /// </summary>
        /// <param name="stuId"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("stuseminfo")]
        [Authorize(Policy = "Wechat")]
        public async Task<object> StuSemInfoAsync(int stuId, int page = 1, int pagesize = 99999)
        {
            var message = new Message<object>();
            try
            {
                var data = await _serviceStuSemInfo.StuSemInfo(stuId, page, pagesize);
                message.data = data;

            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }

        /// <summary>
        /// 当前课程报名学生列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="semid"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("stuinsemlist")]
        [Authorize(Policy = "AdminOrUser")]
        public async Task<object> StuInSemList(string query, int semid, int page, int pagesize = 10)
        {
            var message = new Message<object>();
            try
            {
                var data = await _serviceStuSemInfo.StuInSemViewList(query, semid, page, pagesize);
                message.data = data;

            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }

        /// <summary>
        /// 修改学籍信息
        /// </summary>
        /// <param name="stuSemInfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatestuseminfo")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> UpdateStuSemInfo([FromBody] StuSemInfo stuSemInfo)
        {
            var message = new Message<string>();
            try
            {
                var data = await _serviceStuSemInfo.Update(stuSemInfo, new List<string> { "Times", "Remark" });
                if (data)
                {
                    message.code = 20002;
                    message.msg = "修改成功！";
                }
                else
                {
                    message.code = 20003;
                    message.msg = "修改失败!";
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
        /// <summary>
        /// 添加学籍信息
        /// </summary>
        /// <param name="stuSemInfo"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("addstuseminfo")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> AddStuSemInfo([FromBody] StuSemInfo stuSemInfo)
        {
            var message = new Message<string>();
            try
            {
                var exist = await _serviceStuSemInfo.ExistAsync(stuSemInfo);
                if (exist)
                {
                    message.code = 20003;
                    message.msg = "学籍信息已存在，请修改后再提交！";
                }
                else
                {
                    var data = await _serviceStuSemInfo.Add(stuSemInfo);
                    if (data > 0)
                    {
                        var sem = new Semesters();
                        sem.Id = stuSemInfo.SemId;
                        var semItem = await _serviceSemesters.UpdateSemestersClassAsync(sem);
                        message.code = 20002;
                        message.msg = "添加成功！";
                    }
                    else
                    {
                        message.code = 20003;
                        message.msg = "添加失败!";
                    }
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
        /// <summary>
        /// 删除学籍信息
        /// </summary>
        /// <param name="id">学籍表Id</param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delstuseminfo")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> DelStuSemInfo(int id, bool isDeleted)
        {
            var data = await _serviceStuSemInfo.Update(new StuSemInfo() { Id = id, IsDeleted = isDeleted }, new List<string> { "IsDeleted" });
            if (data)
            {
                return new Message<string>()
                {
                    code = 20002,
                    msg = "删除成功！"
                };
            }
            else
            {
                return new Message<string>()
                {
                    code = 20003,
                    msg = "删除失败!"
                };
            }
        }
        #endregion

        #region 学生上课信息
        /// <summary>
        /// 获取上课信息列表
        /// </summary>
        /// <param name="stuName"></param>
        /// <param name="stuNum"></param>
        /// <param name="semId"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("stusemrecordlist")]
        [Authorize(Policy = "AdminOrUser")]
        public async Task<object> StuSemRecordListAsync(string stuName, string stuNum, int semId, int page, int pagesize = 10)
        {
            var message = new Message<object>();
            try
            {
                var data = await _serviceStuSemRecord.StuSemInfoViewList(stuName, stuNum, semId, page, pagesize);
                message.data = data;

            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }


        /// <summary>
        /// 单个学生上课记录
        /// </summary>
        /// <param name="stuId"></param>
        /// <param name="semId"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("sturecord")]
        [Authorize(Policy = "Wechat")]
        public async Task<object> StuRecord(int stuId, int semId, int page, int pagesize = 20)
        {
            var message = new Message<object>();
            try
            {
                var data = await _serviceStuSemRecord.StuRecordList(stuId, semId, page, pagesize);
                message.data = data;

            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }

        /// <summary>
        /// 修改上课信息
        /// </summary>
        /// <param name="stuSemInfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatestusemrecord")]
        [Authorize(Policy = "AdminOrUser")]
        public async Task<Message<string>> UpdateStuSemRecord([FromBody] StuSemRecord stuSemInfo)
        {
            var message = new Message<string>();
            try
            {
                var data = await _serviceStuSemRecord.Update(stuSemInfo, new List<string> { "Performance", "LearningContent", "Homework", "Remark" });
                if (data)
                {
                    message.code = 20002;
                    message.msg = "修改成功！";
                }
                else
                {
                    message.code = 20003;
                    message.msg = "修改失败!";
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
        /// <summary>
        /// 添加上课信息
        /// </summary>
        /// <param name="stuSemRecord"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("addstusemrecord")]
        [Authorize(Policy = "AdminOrUser")]
        public async Task<Message<string>> AddStuStuSemRecord([FromBody] StuSemRecord stuSemRecord)
        {
            var message = new Message<string>();
            if (stuSemRecord != null)
            {
                try
                {
                    var exist = await _serviceStuSemRecord.ExistAsync(stuSemRecord);
                    if (exist)
                    {
                        message.code = 20003;
                        message.msg = "上课信息已存在，请修改后再提交！";
                    }
                    else
                    {
                        var data = await _serviceStuSemRecord.Add(stuSemRecord);
                        if (data > 0)
                        {
                            var sem = new StuSemInfo();
                            sem.SemId = stuSemRecord.SemId;
                            sem.StuId = stuSemRecord.StuId;
                            var stusem = await _serviceStuSemInfo.UpdateSemestersClassAsync(sem);
                            message.code = 20002;
                            message.msg = "添加成功！";
                        }
                        else
                        {
                            message.code = 20003;
                            message.msg = "添加失败!";
                        }
                    }
                }
                catch (Exception ex)
                {
                    message.code = 20004;
                    message.msg = ex.Message;
                }
            }
            else
            {
                message.code = 20005;
                message.msg = "数据不能为空";
            }

            return message;
        }
        /// <summary>
        /// 删除上课信息
        /// </summary>
        /// <param name="id">上课记录Id</param>
        /// <param name="semId"></param>
        /// <param name="stuId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delstusemrecord")]
        [Authorize(Policy = "AdminOrUser")]
        public async Task<Message<string>> DelStuSemRecord(int id, int semId, int stuId)
        {
            var data = await _serviceStuSemRecord.Update(new StuSemRecord() { Id = id, IsDeleted = true }, new List<string> { "IsDeleted" });
            if (data)
            {
                var sem = new StuSemInfo();
                sem.SemId = semId;
                sem.StuId = stuId;
                var stusem = await _serviceStuSemInfo.ReduceSemestersClassAsync(sem);
                if (stusem > 0)
                {
                    return new Message<string>()
                    {
                        code = 20002,
                        msg = "删除成功！"
                    };
                }
                else
                {
                    return new Message<string>()
                    {
                        code = 20002,
                        msg = "删除成功,更新报课失败，请联系管理员！"
                    };
                }
            }
            else
            {
                return new Message<string>()
                {
                    code = 20003,
                    msg = "删除失败!"
                };
            }
        }
        #endregion
    }
}