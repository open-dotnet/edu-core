﻿using AutoMapper;
using EduCore.Common.Authorization;
using EduCore.Common.Helper;
using EduCore.IServices;
using EduCore.Models;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EduCore.Controllers
{
    /// <summary>
    /// 用户管理
    /// </summary>
    [Produces("application/json")]
    [Route("api/user")]
    public class UserController : Controller
    {

        readonly IServiceUser _serviceUser;
        readonly IServiceStudent _serviceStudent;
        readonly IServiceWechatAuthUser _serviceWechatAuthUser;
        readonly IServiceWechatStudent _serviceWechatStudent;
        readonly IMapper _mapper;

        public UserController(IServiceUser serviceUser,
                              IServiceStudent serviceStudent,
                              IServiceWechatAuthUser serviceWechatAuthUser,
                              IServiceWechatStudent iserviceWechatStudent,
                              IMapper mapper)
        {
            _serviceUser = serviceUser;
            _serviceWechatAuthUser = serviceWechatAuthUser;
            _serviceWechatStudent = iserviceWechatStudent;
            _serviceStudent = serviceStudent;
            _mapper = mapper;
        }

        #region 用户学生信息表
        /// <summary>
        /// 获取学生列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="grade"></param>
        /// <param name="isDeleted"></param>
        /// <param name="page"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("student")]
        [Authorize(Policy = "Admin")]
        public async Task<object> StudentListAsync(string query, string grade, bool? isDeleted, int page, int pagesize = 10)
        {
            var message = new Message<object>();
            try
            {
                PageModel<Student> data = null;
                if (isDeleted == null)
                {
                    if (string.IsNullOrEmpty(query))
                    {
                        if (string.IsNullOrEmpty(grade))
                        {
                            data = await _serviceStudent.QueryPage(null, page, pagesize, "IsDeleted, Id desc ");
                        }
                        else
                        {
                            data = await _serviceStudent.QueryPage(s => s.Grade.Contains(grade), page, pagesize, "IsDeleted, Id desc ");
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(grade))
                        {
                            data = await _serviceStudent.QueryPage(s => s.StuName.Contains(query) || s.Stunum.Contains(query)
                                                                , page, pagesize, "IsDeleted, Id desc ");
                        }
                        else
                        {
                            data = await _serviceStudent.QueryPage(s => s.Grade.Contains(grade)
                                                                    && (s.StuName.Contains(query)
                                                                        || s.Stunum.Contains(query))
                                                                   , page, pagesize, "IsDeleted, Id desc ");

                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(query))
                    {
                        if (string.IsNullOrEmpty(grade))
                        {
                            data = await _serviceStudent.QueryPage(s => s.IsDeleted == isDeleted, page, pagesize, " Id desc ");
                        }
                        else
                        {
                            data = await _serviceStudent.QueryPage(s => s.IsDeleted == isDeleted && s.Grade.Contains(grade), page, pagesize, " Id desc ");
                        }

                    }
                    else
                    {
                        if (string.IsNullOrEmpty(grade))
                        {
                            data = await _serviceStudent.QueryPage(s => (s.StuName.Contains(query) || s.Stunum.Contains(query)) && s.IsDeleted == isDeleted
                                                                , page, pagesize, "IsDeleted, Id desc ");
                        }
                        else
                        {
                            data = await _serviceStudent.QueryPage(s => (s.StuName.Contains(query) || s.Stunum.Contains(query))
                                                                        && s.IsDeleted == isDeleted
                                                                        && s.Grade.Contains(grade)
                                                                        , page, pagesize, "IsDeleted, Id desc ");
                        }
                    }

                }
                message.data = data;
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }

        /// <summary>
        /// 修改学生信息
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatestudent")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> UpdateStudent([FromBody] Student student)
        {
            var message = new Message<string>();
            if (student != null)
            {
                try
                {
                    student.AuthDate = DateTime.Now.AddMinutes(student.AuthMin);
                    var data = await _serviceStudent.Update(student, null, new List<string> { "IsDeleted", "CreateTime" });
                    if (data)
                    {
                        message.code = 20002;
                        message.msg = "修改成功！";
                    }
                    else
                    {
                        message.code = 20003;
                        message.msg = "修改失败!";
                    }
                }
                catch (Exception ex)
                {
                    message.code = 20004;
                    message.msg = ex.Message;
                }
            }
            else
            {
                message.code = 20005;
                message.msg = "不能传空值";
            }

            return message;
        }
        /// <summary>
        /// 添加学生信息
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("addstudent")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> AddStudent([FromBody] Student student)
        {
            var message = new Message<string>();
            try
            {
                var exist = await _serviceStudent.ExistAsync(student);
                if (exist)
                {
                    message.code = 20003;
                    message.msg = "学号已存在，请修改后重试!";
                }
                else
                {
                    student.AuthDate = DateTime.Now.AddMinutes(student.AuthMin);
                    var data = await _serviceStudent.Add(student);
                    if (data > 0)
                    {

                        message.code = 20002;
                        message.msg = "添加成功！";
                    }
                    else
                    {
                        message.code = 20003;
                        message.msg = "添加失败!";
                    }
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
        /// <summary>
        /// 删除学生信息
        /// </summary>
        /// <param name="id">学生表Id</param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delstudent")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> DelStudent(int id, bool isDeleted)
        {
            var message = new Message<string>();
            try
            {
                var data = await _serviceStudent.Update(new Student() { Id = id, IsDeleted = isDeleted }, new List<string> { "IsDeleted" });
                if (data)
                {
                    message.code = 20002;
                    message.msg = "删除成功！";
                }
                else
                {
                    message.code = 20003;
                    message.msg = "删除失败!";
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }

        /// <summary>
        /// 微信绑定学生信息
        /// </summary>
        /// <param name="student"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("addstuwechat")]
        [Authorize(Policy = "Wechat")]
        public async Task<Message<string>> AddStuWechat([FromBody]Student student)
        {
            var message = new Message<string>();
            try
            {
                var tokenstr = HttpContext.Request.Headers["Authorization"];
                if (string.IsNullOrEmpty(tokenstr))
                {
                    message.code = 50008;
                    message.msg = "无授权信息";
                }
                else
                {
                    var token = tokenstr.ToString().Replace("Bearer ", "");
                    var tokenModel = JwtHelper.SerializeWechatJwt(token);
                    if (tokenModel != null && !string.IsNullOrEmpty(tokenModel.Uid))
                    {
                        var students = await _serviceStudent.Query(stu => stu.Stunum == student.Stunum);
                        if (students != null && students.Count > 0)
                        {
                            var stu = students[0];
                            if (stu.AuthDate.HasValue && stu.AuthDate >= DateTime.Now)
                            {
                                var openid = tokenModel.Uid;
                                var exsit = await _serviceWechatStudent.ExistAsync(openid, stu.Id);
                                if (exsit)
                                {
                                    message.code = 20003;
                                    message.msg = "当前学生已绑定";
                                }
                                else
                                {
                                    WechatStudent wechatStudent = new WechatStudent
                                    {
                                        Openid = openid,
                                        StuId = stu.Id
                                    };
                                    var resutl = await _serviceWechatStudent.Add(wechatStudent);
                                    message.code = 20000;
                                    message.msg = "绑定成功";
                                }
                            }
                            else
                            {
                                message.code = 20003;
                                message.msg = "当前学生授权绑定时间已过，请联系教育机构！";
                            }
                        }
                        else
                        {
                            message.code = 20003;
                            message.msg = "当前学号不存在，请检查!";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
        /// <summary>
        /// 解绑微信学生
        /// </summary>
        /// <param name="wechatStudent"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("delstuwechat")]
        [Authorize(Policy = "Wechat")]
        public async Task<Message<string>> DelStuWechat([FromBody]WechatStudent wechatStudent)
        {
            var message = new Message<string>();
            try
            {
                var tokenstr = HttpContext.Request.Headers["Authorization"];
                if (string.IsNullOrEmpty(tokenstr))
                {
                    message.code = 50008;
                    message.msg = "无授权信息";
                }
                else
                {
                    var token = tokenstr.ToString().Replace("Bearer ", "");
                    var tokenModel = JwtHelper.SerializeWechatJwt(token);
                    if (tokenModel != null && !string.IsNullOrEmpty(tokenModel.Uid))
                    {
                        var result = await _serviceWechatStudent.DeleteById(wechatStudent.Id);
                        if (result)
                        {
                            message.code = 20000;
                            message.msg = "解绑成功";
                        }
                        else
                        {
                            message.code = 20003;
                            message.msg = "解绑失败，请刷新后再操作";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }

        #endregion


        /// <summary>
        /// 普通用户登录信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("userinfo")]
        [Authorize(Policy = "AdminOrUser")]
        public async Task<Message<object>> UserInfoAsync(string token)
        {
            var message = new Message<object>();

            if (!string.IsNullOrEmpty(token))
            {
                var tokenModel = JwtHelper.SerializeJwt(token);
                if (tokenModel != null && tokenModel.Uid > 0)
                {
                    var result = await _serviceUser.QueryById(tokenModel.Uid);
                    message.code = 20000;
                    message.msg = "ok";
                    message.data = new
                    {
                        id = result.Id,
                        roles = new string[] { Enum.GetName(typeof(EnumHelper.Roles), result.Role) },
                        name = result.NickName
                    };
                }
                else
                {
                    message.code = 50008;
                    message.msg = "error";
                    message.data = null;
                }

            }
            else
            {
                message.code = 20001;
                message.msg = "error";
                message.data = null;
            }
            return message;
        }

        /// <summary>
        /// 微信用户登录信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("wechatinfo")]
        [Authorize(Policy = "Wechat")]
        public async Task<Message<object>> WechatInfoAsync()
        {
            var message = new Message<object>();
            {
                var tokenstr = HttpContext.Request.Headers["Authorization"];
                if (string.IsNullOrEmpty(tokenstr))
                {
                    message.code = 50008;
                    message.msg = "无授权信息";
                }
                else
                {
                    var token = tokenstr.ToString().Replace("Bearer ", "");
                    var tokenModel = JwtHelper.SerializeWechatJwt(token);
                    if (tokenModel != null && !string.IsNullOrEmpty(tokenModel.Uid))
                    {
                        var result = await _serviceWechatAuthUser.WechatStudentList(tokenModel.Uid, 1, 999);
                        if (result != null && result.data != null && result.data.Count > 0)
                        {
                            message.code = 20000;
                            message.msg = "ok";
                            message.data = result.data;
                        }
                        else
                        {
                            message.code = 20002;
                            message.msg = "当前用户没有绑定学生信息";
                        }
                    }
                    else
                    {
                        message.code = 50008;
                        message.msg = "Token错误";
                        message.data = null;
                    }
                }
                return message;
            }
        }


        [HttpGet]
        [Route("userlist")]
        [Authorize(Policy = "Admin")]
        public async Task<object> UserList(bool? isDeleted, int page, int pagesize = 10)
        {
            var message = new Message<object>();
            try
            {
                PageModel<User> data = null;
                if (isDeleted == null)
                    data = await _serviceUser.QueryPage(null, page, pagesize, "IsDeleted, Id desc ");
                else
                    data = await _serviceUser.QueryPage(s => s.IsDeleted == isDeleted
                                                            , page, pagesize, "IsDeleted, Id desc ");
                message.data = _mapper.Map<PageModel<UserView>>(data);
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateuser")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> UpdateUser([FromBody] User user)
        {
            var message = new Message<string>();
            if (user != null)
            {
                try
                {
                    user.UpdateTime = DateTime.Now;
                    var data = await _serviceUser.Update(user, null, new List<string> { "Password", "IsDeleted", "CreateTime" });
                    if (data)
                    {
                        message.code = 20002;
                        message.msg = "修改成功！";
                    }
                    else
                    {
                        message.code = 20003;
                        message.msg = "修改失败!";
                    }
                }
                catch (Exception ex)
                {
                    message.code = 20004;
                    message.msg = ex.Message;
                }
            }
            else
            {
                message.code = 20005;
                message.msg = "不能传空值";
            }

            return message;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatepassword")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> UpdatePassword([FromBody] User user)
        {
            var message = new Message<string>();
            if (user != null)
            {
                try
                {
                    user.UpdateTime = DateTime.Now;
                    user.Password = MD5Helper.MD5Encrypt64(user.Password);
                    var data = await _serviceUser.Update(user, new List<string> { "Password" }, null);
                    if (data)
                    {
                        message.code = 20002;
                        message.msg = "修改成功！";
                    }
                    else
                    {
                        message.code = 20003;
                        message.msg = "修改失败!";
                    }
                }
                catch (Exception ex)
                {
                    message.code = 20004;
                    message.msg = ex.Message;
                }
            }
            else
            {
                message.code = 20005;
                message.msg = "不能传空值";
            }

            return message;
        }
        /// <summary>
        /// 添加用户信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("adduser")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> AddUser([FromBody] User user)
        {
            var message = new Message<string>();
            try
            {
                var exist = await _serviceUser.ExistAsync(user);
                if (exist)
                {
                    message.code = 20003;
                    message.msg = "用户名已存在，请修改后再提交";
                }
                else
                {
                    user.Password = MD5Helper.MD5Encrypt64(user.Password);
                    var data = await _serviceUser.Add(user);
                    if (data > 0)
                    {

                        message.code = 20002;
                        message.msg = "添加成功！";
                    }
                    else
                    {
                        message.code = 20003;
                        message.msg = "添加失败!";
                    }
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
        /// <summary>
        /// 删除用户信息
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deluser")]
        [Authorize(Policy = "Admin")]
        public async Task<Message<string>> DelUser(int id, bool isDeleted)
        {
            var message = new Message<string>();
            try
            {
                var data = await _serviceUser.Update(new User() { Id = id, IsDeleted = isDeleted }, new List<string> { "IsDeleted" });
                if (data)
                {
                    message.code = 20002;
                    message.msg = "删除成功！";
                }
                else
                {
                    message.code = 20003;
                    message.msg = "删除失败!";
                }
            }
            catch (Exception ex)
            {
                message.code = 20004;
                message.msg = ex.Message;
            }
            return message;
        }
    }
}