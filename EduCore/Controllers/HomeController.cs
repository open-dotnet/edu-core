﻿using EduCore.IServices;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EduCore.Controllers
{
    [Route("[controller]")]
    public class HomeController : Controller
    {
        private ILogService LogService { get; set; }



        public HomeController(ILogService logService)
        {
            LogService = logService;
        }

        /// <summary>
        /// 首页
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(int id)
        {
            return Json(0);
        }

    }
}