﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using EduCore.AOP;
using EduCore.Common;
using EduCore.Common.Log;
using EduCore.Common.LogHelper;
using EduCore.Extensions;
using EduCore.Filter;
using EduCore.IServices;
using EduCore.Service;
using log4net;
using log4net.Config;
using log4net.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Senparc.CO2NET;
using Senparc.Weixin.Entities;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace EduCore
{
  public class Startup
  {
    public static ILoggerRepository repository { get; set; }
    public IConfiguration Configuration { get; }
    public IWebHostEnvironment Env { get; }

    public Startup(IConfiguration configuration, IWebHostEnvironment env)
    {
      Configuration = configuration;
      Env = env;
      var contentPath = env.ContentRootPath;
      var log4Config = Path.Combine(contentPath, "log4net.config");
      repository = LogManager.CreateRepository("Wechat");
      XmlConfigurator.ConfigureAndWatch(repository, new FileInfo(log4Config));
    }

    public void ConfigureServices(IServiceCollection services)
    {
      services.Configure<IISServerOptions>(options =>
      {
        options.AutomaticAuthentication = false;
      });
      services.Configure<CookiePolicyOptions>(options =>
      {
        // This lambda determines whether user consent for non-essential cookies is needed for a given request.
        options.CheckConsentNeeded = context => false;
        options.MinimumSameSitePolicy = SameSiteMode.None;
      });
      services.AddSingleton<ILogService, LogService>();//普通日志
      services.AddSingleton<IAppLogService, AppLogService>();//api日志，系统错误日志
      services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
      services.AddMemoryCache();//使用本地缓存必须添加
      services.AddDbSetup();
      services.AddSqlsugarSetup();
      services.AddAutoMapperSetup();
      services.AddCorsSetup();

      #region 【第二步：配置认证服务】
      var audienceConfig = Configuration.GetSection("Audience");
      var symmetricKeyAsBase64 = audienceConfig["Secret"];
      var keyByteArray = Encoding.ASCII.GetBytes(symmetricKeyAsBase64);
      var signingKey = new SymmetricSecurityKey(keyByteArray);

      // 令牌验证参数
      var tokenValidationParameters = new TokenValidationParameters
      {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = signingKey,
        ValidateIssuer = true,
        ValidIssuer = audienceConfig["Issuer"],//发行人
        ValidateAudience = true,
        ValidAudience = audienceConfig["Audience"],//订阅人
                                                   // 是否验证Token有效期，使用当前时间与Token的Claims中的NotBefore和Expires对比
        ValidateLifetime = true,
        // 允许的服务器时间偏移量
        ClockSkew = TimeSpan.FromHours(2),
        // 是否要求Token的Claims中必须包含Expires
        RequireExpirationTime = true,
      };

      //2.1【认证】、core自带官方JWT认证
      services.AddAuthentication(x =>
      {
        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
      })
       .AddJwtBearer(o =>
       {
         o.TokenValidationParameters = tokenValidationParameters;
         o.Events = new JwtBearerEvents
         {
           OnAuthenticationFailed = context =>
               {
                 // 如果过期，则把<是否过期>添加到，返回头信息中
                 if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                 {
                   context.Response.Headers.Add("Token-Expired", "true");
                   context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                 }
                 return Task.CompletedTask;
               }
         };
       });

      services.AddAuthorization(options =>
      {
        options.AddPolicy("Wechat", policy => policy.RequireRole("wechat").Build());
        options.AddPolicy("User", policy => policy.RequireRole("user").Build());
        options.AddPolicy("Admin", policy => policy.RequireRole("admin").Build());
        options.AddPolicy("AdminOrUser", policy => policy.RequireRole("admin", "user"));
      });

      #endregion
      services.AddSingleton(new Appsettings(Env));
      services.AddSingleton(new LogLock(Env));
      services.AddSingleton<ILoggerHelper, LogHelper>();
      services.AddSwaggerSetup();
      services.AddControllers(m =>
      {
        m.Filters.Add(typeof(GlobalExceptionsFilter));
      })
           .AddNewtonsoftJson(options =>
           {
             options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
             options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
           });
      services.Configure<FormOptions>(x =>
      {
        x.ValueLengthLimit = int.MaxValue;
        x.MultipartBodyLengthLimit = long.MaxValue;
        x.MemoryBufferThreshold = int.MaxValue;
      });
    }
    public void ConfigureContainer(ContainerBuilder builder)
    {
      builder.RegisterModule<CoreRegisterModule>();
    }
    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app,
                          IWebHostEnvironment env,
                          IOptions<SenparcSetting> senparcSetting,
                          IOptions<SenparcWeixinSetting> senparcWeixinSetting)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseExceptionHandler("/Home/Error");
      }
      app.UseRouting();
      app.UseCors("LimitRequests");
      app.UseStaticFiles();
      app.UseCookiePolicy();
      // 先开启认证
      app.UseAuthentication();
      // 然后是授权中间件
      app.UseAuthorization();
      app.UseStaticHttpContext();
      #region Swagger
      app.UseSwagger();
      if (Appsettings.app(new string[] { "UseSwagger" }).ObjToBool())
      {
        app.UseSwaggerUI(c =>
        {
          //根据版本名称倒序 遍历展示
          c.SwaggerEndpoint($"/swagger/v1/swagger.json", "Netcore");

          // 将swagger首页，设置成我们自定义的页面，记得这个字符串的写法：解决方案名.index.html
          //c.IndexStream = () => GetType().GetTypeInfo().Assembly.GetManifestResourceStream("Blog.Core.index.html");//这里是配合MiniProfiler进行性能监控的，《文章：完美基于AOP的接口性能分析》，如果你不需要，可以暂时先注释掉，不影响大局。
          c.RoutePrefix = ""; //路径配置，设置为空，表示直接在根域名（localhost:8001）访问该文件,注意localhost:8001/swagger是访问不到的，去launchSettings.json把launchUrl去掉，如果你想换一个路径，直接写名字即可，比如直接写c.RoutePrefix = "doc";
        });
      }

      #endregion
      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllerRoute(
                  name: "default",
                  pattern: "{controller=Home}/{action=Index}/{id?}");
      });
    }
  }
}
