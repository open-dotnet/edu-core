﻿using AutoMapper;
using EduCore.Common.Helper;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;

namespace EduCore.AutoMapper
{
  public class CustomProfile : Profile
  {
    /// <summary>
    /// 配置构造函数，用来创建关系映射
    /// </summary>
    public CustomProfile()
    {
      CreateMap<UserView, User>();
      CreateMap<User, UserView>();
      CreateMap<PageModel<UserView>, PageModel<User>>();
      CreateMap<PageModel<User>, PageModel<UserView>>();
    }
  }
}
