﻿using System;
using Autofac.Extensions.DependencyInjection;
using EduCore.Common;
using EduCore.Models.Seed;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace EduCore
{
  public class Program
  {
    public static void Main(string[] args)
    {
      //CreateWebHostBuilder(args).Build().Run();
      // 生成承载 web 应用程序的 Microsoft.AspNetCore.Hosting.IWebHost。Build是WebHostBuilder最终的目的，将返回一个构造的WebHost，最终生成宿主。
      var host = CreateHostBuilder(args).Build();
      //创建可用于解析作用域服务的新 Microsoft.Extensions.DependencyInjection.IServiceScope。
      using (var scope = host.Services.CreateScope())
      {
        var services = scope.ServiceProvider;
        var loggerFactory = services.GetRequiredService<ILoggerFactory>();
        try
        {
          // 从 system.IServicec提供程序获取 T 类型的服务。
          // 为了大家的数据安全，这里先注释掉了，大家自己先测试玩一玩吧。
          // 数据库连接字符串是在 Model 层的 Seed 文件夹下的 MyContext.cs 中
          var configuration = services.GetRequiredService<IConfiguration>();
          if (configuration.GetSection("AppSettings")["SeedDBEnabled"].ObjToBool())
          {
            var myContext = services.GetRequiredService<MyContext>();
            DBSeed.SeedAsync(myContext);
          }
        }
        catch (Exception e)
        {
          var logger = loggerFactory.CreateLogger<Program>();
          logger.LogError(e, "Error occured seeding the Database.");
          throw;
        }
      }
      host.Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .UseServiceProviderFactory(new AutofacServiceProviderFactory()) //<--NOTE THIS
            .ConfigureWebHostDefaults(webBuilder =>
            {
              webBuilder
                  .ConfigureKestrel(serverOptions =>
                  {
                    serverOptions.AllowSynchronousIO = true;//启用同步 IO
                    serverOptions.Limits.MaxRequestBodySize = 524288000;
                  })
                  .UseStartup<Startup>()
                  .ConfigureLogging((hostingContext, builder) =>
                  {
                    builder.ClearProviders();
                    builder.SetMinimumLevel(LogLevel.Trace);
                    // builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    builder.AddConsole();
                    builder.AddDebug();
                    //可配置文件
                    //过滤掉系统默认的一些日志
                    builder.AddFilter("System", LogLevel.Error);
                    builder.AddFilter("Microsoft", LogLevel.Error);
                    builder.AddFilter("Blog.Core.AuthHelper.ApiResponseHandler", LogLevel.Error);
                    builder.AddLog4Net();
                  });
            });
  }
}
