﻿using EduCore.Services.BASE;
using EduCore.IServices;
using EduCore.Models.Model;

namespace EduCore.Services
{
    /// <summary>
    /// 用户跟角色关联表
    /// </summary>
    public class ServiceUserRole : BaseServices<UserRole>, IServiceUserRole
    {

    }
}
