﻿using log4net;

namespace InfoMedia.Core.Services.Implement
{
    public class WeixinLogService : IWeixinLogService
    {
        private readonly ILog _weixinlog = LogManager.GetLogger("Winxin.Logging");

        public void InfoWeixin(object msg)
        {
            _weixinlog.Info(msg);
        }

        public void ErrorWeixin(object msg)
        {
            _weixinlog.Error(msg);
        }
    }
}