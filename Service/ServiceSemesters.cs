﻿using EduCore.Services.BASE;
using EduCore.IRepository;
using EduCore.IServices;
using EduCore.Models.Model;
using System.Linq;
using System.Threading.Tasks;

namespace EduCore.Services
{
    /// <summary>
    /// 学期表
    /// </summary>
    public class ServiceSemesters : BaseServices<Semesters>, IServiceSemesters
    {
        IRepositorySemesters _dal;
        public ServiceSemesters(IRepositorySemesters dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }

        public async Task<bool> ExistAsync(Semesters semesters)
        {
            var exist = false;

            var item = (await base.Query(s => s.Title == semesters.Title)).FirstOrDefault();

            if (item != null)
            {
                exist = true;
            }
            return exist;
        }

        public Task<int> UpdateSemestersClassAsync(Semesters semesters)
        {
            return _dal.UpdateSemestersClassAsync(semesters);
        }
    }
}
