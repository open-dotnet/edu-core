﻿using EduCore.Services.BASE;
using EduCore.IRepository;
using EduCore.IServices;
using EduCore.Models.Model;
using System.Linq;
using System.Threading.Tasks;

namespace EduCore.Services
{
    /// <summary>
    /// 学生信息表
    /// </summary>
    public class ServiceStudent : BaseServices<Student>, IServiceStudent
    {
        IRepositoryStudent _dal;
        public ServiceStudent(IRepositoryStudent dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }

        public async Task<bool> ExistAsync(Student student)
        {
            var exist = false;

            var item = (await base.Query(s => s.Stunum == student.Stunum)).FirstOrDefault();

            if (item != null)
            {
                exist = true;
            }
            return exist;
        }
    }
}
