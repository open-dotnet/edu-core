﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EduCore.Services.BASE;
using EduCore.IRepository;
using EduCore.IServices;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;

namespace EduCore.Services
{
    /// <summary>
    /// 学期表
    /// </summary>
    public class ServiceStuSemRecord : BaseServices<StuSemRecord>, IServiceStuSemRecord
    {
        IRepositoryStuSemRecord _dal;
        public ServiceStuSemRecord(IRepositoryStuSemRecord dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }

        public async Task<bool> ExistAsync(StuSemRecord stuSemRecord)
        {
            var exist = false;

            var item = (await base.Query(s => s.StuId == stuSemRecord.StuId
                                        && s.SemId == stuSemRecord.SemId
                                        && s.IsDeleted == false
                                        && s.ClassStart == stuSemRecord.ClassStart)).FirstOrDefault();

            if (item != null)
            {
                exist = true;
            }
            return exist;
        }

        public async Task<PageModel<StuSemRecordView>> StuSemInfoViewList(string stuName, string stuNum, int semId, int intPageIndex = 1, int intPageSize = 20)
        {
            var item = await _dal.StuSemRecordViewList(stuName, stuNum, semId, intPageIndex, intPageSize);
            return item;
        }

        public async Task<PageModel<StuSemRecordView>> StuRecordList(int stuId, int semId, int intPageIndex = 1, int intPageSize = 20)
        {
            var item = await _dal.StuRecordList(stuId, semId, intPageIndex, intPageSize);
            return item;
        }
    }
}
