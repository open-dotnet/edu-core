﻿using EduCore.Services.BASE;
using EduCore.IServices;
using EduCore.Models.Model;
using EduCore.IRepository;
using System.Threading.Tasks;
using EduCore.Models.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace EduCore.Services
{
    public class ServiceWechatAuthUser : BaseServices<WechatAuthUser>, IServiceWechatAuthUser
    {
        IRepositoryWechatAuthUser _dal;
        public ServiceWechatAuthUser(IRepositoryWechatAuthUser dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }
        public async Task<bool> ExistAsync(string openid)
        {
            var exist = false;

            var item = (await base.Query(s => s.Openid == openid)).FirstOrDefault();
            if (item != null)
            {
                exist = true;
            }
            return exist;
        }
        public async Task<PageModel<WechatStudentView>> WechatStudentList(string openid, int intPageIndex = 1, int intPageSize = 20)
        {
            return await _dal.WechatStudentList(openid, intPageIndex, intPageSize);
        }

        public async Task<List<WechatAuthUser>> WechatUser(string username)
        {
            return await _dal.Query(u => u.Nickname.Contains(username));
        }
    }
}
