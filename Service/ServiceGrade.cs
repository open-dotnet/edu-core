﻿using EduCore.Services.BASE;
using EduCore.IRepository;
using EduCore.IServices;
using EduCore.Models.Model;
using System.Linq;
using System.Threading.Tasks;

namespace EduCore.Services
{
    public class ServiceGrade : BaseServices<Grade>, IServiceGrade
    {
        IRepositoryGrade _dal;
        public ServiceGrade(IRepositoryGrade dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }

        public async Task<bool> ExistAsync(Grade grade, bool except = false)
        {
            var exist = false;
            Grade item = null;
            if (except)
            {
                item = (await base.Query(s => s.Name == grade.Name && s.Id != grade.Id)).FirstOrDefault();
            }
            else
            {
                item = (await base.Query(s => s.Name == grade.Name)).FirstOrDefault();
            }
            if (item != null)
            {
                exist = true;
            }
            return exist;
        }

    }
}
