﻿using EduCore.Services.BASE;
using EduCore.IRepository;
using EduCore.IServices;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EduCore.Services
{
    /// <summary>
    /// 学生学籍信息
    /// </summary>
    public class ServiceStuSemInfo : BaseServices<StuSemInfo>, IServiceStuSemInfo
    {
        IRepositoryStuSemInfo _dal;
        public ServiceStuSemInfo(IRepositoryStuSemInfo dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }

        public async Task<bool> ExistAsync(StuSemInfo stuSemInfo)
        {
            var exist = false;

            var item = (await base.Query(s => s.StuId == stuSemInfo.StuId
                                        && s.SemId == stuSemInfo.SemId)).FirstOrDefault();

            if (item != null)
            {
                exist = true;
            }
            return exist;
        }

        public async Task<int> UpdateSemestersClassAsync(StuSemInfo stuSemInfo)
        {
            var item = await _dal.UpdateSemestersClassAsync(stuSemInfo);
            return item;
        }

        public async Task<PageModel<StuInSemView>> StuInSemViewList(string query, int semid, int intPageIndex = 1, int intPageSize = 20)
        {
            var item = await _dal.StuInSemViewListAsync(query, semid, intPageIndex, intPageSize);
            return item;
        }

        public async Task<PageModel<StuSemInfoView>> StuSemInfoViewList(string stuName, string stuNum, int semId, int intPageIndex = 1, int intPageSize = 20)
        {
            var item = await _dal.StuSemInfoViewList(stuName, stuNum, semId, intPageIndex, intPageSize);
            return item;
        }

        public async Task<int> ReduceSemestersClassAsync(StuSemInfo stuSemInfo)
        {
            var item = await _dal.ReduceSemestersClassAsync(stuSemInfo);
            return item;
        }

        public async Task<PageModel<StuSemInfoView>> StuSemInfo(int stuId, int intPageIndex = 1, int intPageSize = 20)
        {
            var item = await _dal.StuSemInfo(stuId, intPageIndex, intPageSize);
            return item;
        }
    }
}
