﻿using EduCore.Services.BASE;
using EduCore.IServices;
using EduCore.Models.Model;
using EduCore.IRepository;
using System.Threading.Tasks;
using System.Linq;
using EduCore.Models.ViewModel;

namespace EduCore.Services
{
    public class ServiceWechatStudent : BaseServices<WechatStudent>, IServiceWechatStudent
    {

        IRepositoryWechatStudent _dal;
        public ServiceWechatStudent(IRepositoryWechatStudent dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }

        public async Task<bool> ExistAsync(string openid, int stuId)
        {
            var exist = false;

            var item = (await base.Query(s => s.StuId == stuId && s.Openid == openid)).FirstOrDefault();
            if (item != null)
            {
                exist = true;
            }
            return exist;
        }

        public async Task<PageModel<WechatStudentInfoView>> WechatStudentInfoList(string nickname = null, string stuname = null, int intPageIndex = 1, int intPageSize = 20)
        { 
            return await _dal.WechatStudentInfoList(nickname, stuname, intPageIndex, intPageSize);
        }
    }
}
