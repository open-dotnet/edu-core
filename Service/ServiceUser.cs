﻿using System.Linq;
using System.Threading.Tasks;
using EduCore.Services.BASE;
using EduCore.IRepository;
using EduCore.IServices;
using EduCore.Models.Model;

namespace EduCore.Services
{
    /// <summary>
    /// 通过OAuth的获取到的用户信息（snsapi_userinfo=scope）
    /// </summary>
    public class ServiceUser : BaseServices<User>, IServiceUser
    {
        IRepositoryUser _dal;
        public ServiceUser(IRepositoryUser dal)
        {
            this._dal = dal;
            base.BaseDal = dal;
        }

        public async Task<bool> ExistAsync(User user)
        {
            var exist = false;

            var item = (await base.Query(s => s.UserName == user.UserName)).FirstOrDefault();

            if (item != null)
            {
                exist = true;
            }
            return exist;
        }
    }
}
