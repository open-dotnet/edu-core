using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EduCore.IRepository.BASE;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;

namespace EduCore.IRepository
{
    /// <summary>
    /// IUserRoleRepository
    /// </summary>	
    public interface IRepositorySemesters : IBaseRepository<Semesters>
    {
        Task<int> UpdateSemestersClassAsync(Semesters semesters);
    }

    public interface IRepositoryStudent : IBaseRepository<Student>
    {


    }
    public interface IRepositoryStuSemInfo : IBaseRepository<StuSemInfo>
    {
        Task<int> UpdateSemestersClassAsync(StuSemInfo stuSemInfo);
        Task<int> ReduceSemestersClassAsync(StuSemInfo stuSemInfo);
        Task<PageModel<StuSemInfoView>> StuSemInfoViewList(string stuName, string stuNum, int semId, int intPageIndex = 1, int intPageSize = 20);
        Task<PageModel<StuSemInfoView>> StuSemInfo(int stuId, int intPageIndex = 1, int intPageSize = 20);
        Task<PageModel<StuInSemView>> StuInSemViewListAsync(string query, int semid, int intPageIndex = 1, int intPageSize = 20);
    }
    public interface IRepositoryStuSemRecord : IBaseRepository<StuSemRecord>
    {
        Task<PageModel<StuSemRecordView>> StuSemRecordViewList(string stuName, string stuNum, int semId, int intPageIndex = 1, int intPageSize = 20);
        Task<PageModel<StuSemRecordView>> StuRecordList(int stuId, int semId, int intPageIndex = 1, int intPageSize = 20);

    }
    public interface IRepositoryUser : IBaseRepository<User>
    {


    }
    public interface IRepositoryUserRole : IBaseRepository<UserRole>
    {


    }
    public interface IRepositoryWechatAuthUser : IBaseRepository<WechatAuthUser>
    {
        Task<PageModel<WechatStudentView>> WechatStudentList(string openid, int intPageIndex = 1, int intPageSize = 20);
    }
    public interface IRepositoryWechatStudent : IBaseRepository<WechatStudent>
    {
        Task<PageModel<WechatStudentInfoView>> WechatStudentInfoList(string nickname = null, string stuname = null, int intPageIndex = 1, int intPageSize = 20);
    }
    public interface IRepositorySubject : IBaseRepository<Subject>
    {


    }
    public interface IRepositoryGrade : IBaseRepository<Grade>
    {


    }

}

