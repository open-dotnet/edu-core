﻿
using System;

namespace EduCore.Models.ViewModel
{
    /// <summary>
    /// 学生学籍信息
    /// </summary>
    public class StuInSemView
    {
        /// <summary>
        /// 学生Id
        /// </summary>
        public int StudentId { get; set; }

        ///// <summary>
        /// 学号
        /// </summary>
        public string Stunum { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string StuName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public int Sex { get; set; }

    }
}
