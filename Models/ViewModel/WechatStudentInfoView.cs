﻿
namespace EduCore.Models.ViewModel
{
    /// <summary>
    /// 通过OAuth的获取到的用户信息（snsapi_userinfo=scope）
    /// </summary>
    public class WechatStudentInfoView
    {

        public int WechatStudentId { get; set; }

        public int StudentId { get; set; }

        ///// <summary>
        /// 用户的唯一标识
        /// </summary>
        public string Openid { get; set; }
        /// <summary>
        /// 用户昵称
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空
        /// </summary>
        public string Headimgurl { get; set; }
        ///// <summary>
        /// 学号
        /// </summary>
        public string Stunum { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string StuName { get; set; }

    }
}
