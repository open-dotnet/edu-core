﻿
using System;

namespace EduCore.Models.ViewModel
{
    /// <summary>
    /// 学期表
    /// </summary>
    public class SemestersView
    {
        /// <summary>
        /// 学年
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// 自定义学期名，如：春季
        /// </summary>
        public string SemName { get; set; }

        /// <summary>
        /// 当期年级
        /// </summary>
        public string Grade { get; set; }
        /// <summary>
        /// 学科名
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// 班级数
        /// </summary>
        public int ClassNum { get; set; }

        /// <summary>
        /// 班级学员数
        /// </summary>
        public int ClassSize { get; set; }


        /// <summary>
        /// 当前已报名学员数
        /// </summary>
        public int CurClassSize { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// 学期备注
        /// </summary>        
        public string Remark { get; set; }
    }
}
