﻿
namespace EduCore.Models.ViewModel
{
    /// <summary>
    /// 通过OAuth的获取到的用户信息（snsapi_userinfo=scope）
    /// </summary>
    public class WechatStudentView
    {

        public int WechatStudentId { get; set; }

        public int StudentId { get; set; }


        ///// <summary>
        /// 学号
        /// </summary>
        public string Stunum { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string StuName { get; set; }

    }
}
