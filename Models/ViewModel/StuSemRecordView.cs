﻿
using System;

namespace EduCore.Models.ViewModel
{
    /// <summary>
    /// 学期表
    /// </summary>
    public class StuSemRecordView
    {
        public int StuSemRecordId { get; set; }
        public int StuSemRecordSemId { get; set; }
        public int StuSemRecordStuId { get; set; }

        public bool StuSemRecordIsDeleted { get; set; }

        public DateTime StuSemRecordCreateTime { get; set; }
        /// <summary>
        /// 当前课上课时间
        /// </summary>
        public DateTime ClassStart { get; set; }
        /// <summary>
        /// 当前课下课时间
        /// </summary>
        public DateTime ClassEnd { get; set; }
        /// <summary>
        /// 当前记录创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 上课表现
        /// </summary>
        public string Performance { get; set; }
        /// <summary>
        /// 学习内容
        /// </summary>
        public string LearningContent { get; set; }
        /// <summary>
        /// 学生作业
        /// </summary>
        public string Homework { get; set; }
        ///// <summary>
        /// 学号
        /// </summary>
        public string Stunum { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string StuName { get; set; }
        /// <summary>
        /// 学期名
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 学年
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// 自定义学期名，如：春季
        /// </summary>
        public string SemName { get; set; }

        /// <summary>
        /// 当期年级
        /// </summary>
        public string SemestersGrade { get; set; }
        /// <summary>
        /// 学科名
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// 班级数
        /// </summary>
        public int ClassNum { get; set; }

    }
}
