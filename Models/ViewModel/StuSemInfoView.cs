﻿
using System;

namespace EduCore.Models.ViewModel
{
    /// <summary>
    /// 学生学籍信息
    /// </summary>
    public class StuSemInfoView
    {
        public int StuSemInfoId { get; set; }

        public string StuSemInfoRemark { get; set; }

        public bool StuSemInfoIsDeleted { get; set; }

        public DateTime StuSemInfoCreateTime { get; set; }
        /// <summary>
        /// 学生Id
        /// </summary>
        public int StuId { get; set; }

        /// <summary>
        /// 学期Id
        /// </summary>
        public int SemId { get; set; }
        /// <summary>
        /// 当期次数
        /// </summary>
        public int Times { get; set; }
        /// <summary>
        /// 当期剩余次数
        /// </summary>
        public int UsedTimes { get; set; }

        ///// <summary>
        /// 学号
        /// </summary>
        public string Stunum { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string StuName { get; set; }
        /// <summary>
        /// 学期名
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 学年
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// 自定义学期名，如：春季
        /// </summary>
        public string SemName { get; set; }

        /// <summary>
        /// 当期年级
        /// </summary>
        public string SemestersGrade { get; set; }
        /// <summary>
        /// 学科名
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// 班级数
        /// </summary>
        public int ClassNum { get; set; }

    }
}
