﻿
namespace EduCore.Models.ViewModel
{
    /// <summary>
    /// 学生信息表
    /// </summary>
    public class StudentView
    {
        ///// <summary>
        /// 学号
        /// </summary>
        public string Stunum { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        public string StuName { get; set; }
        /// <summary>
        /// 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
        /// </summary>
        public int Sex { get; set; }
        /// <summary>
        /// 学校
        /// </summary>
        public string School { get; set; }
        /// <summary>
        /// 入学年级
        /// </summary>
        public string Grade { get; set; }
        /// <summary>
    }
}
