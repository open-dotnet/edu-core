﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EduCore.Models.ViewModel
{
    public class UserLoginView
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
