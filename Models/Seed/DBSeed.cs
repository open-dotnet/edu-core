﻿using EduCore.Models.Model;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace EduCore.Models.Seed
{
    public class DBSeed
    {
        /// <summary>
        /// 异步添加种子数据
        /// </summary>
        /// <param name="myContext"></param>
        /// <returns></returns>
        public static async Task SeedAsync(MyContext myContext)
        {
            try
            {
                // 如果生成过了，第二次，就不用再执行一遍了,注释掉该方法即可

                // 自动创建数据库，注意版本是 sugar 5.x 版本的
                myContext.Db.DbMaintenance.CreateDatabase();
                // 创建表
                myContext.Db.CodeFirst.InitTables(
                    typeof(UserRole),
                    typeof(WechatAuthUser),
                    typeof(Student),
                    typeof(StuSemInfo),
                    typeof(StuSemRecord),
                    typeof(Semesters),
                    typeof(WechatStudent),
                    typeof(Grade),
                    typeof(Subject),
                    typeof(User)
                    );

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
