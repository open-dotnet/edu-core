﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EduCore.Models
{
    /// <summary>
    /// 通用返回信息类
    /// </summary>
    public class Message<T>
    {
        /// <summary>
        /// 操作是否成功
        /// </summary>
        public int code { get; set; } = 20000;
        /// <summary>
        /// 返回信息
        /// </summary>
        public string msg { get; set; } = "Ok";
        /// <summary>
        /// 返回数据集合
        /// </summary>
        public T data { get; set; }

    }
}
