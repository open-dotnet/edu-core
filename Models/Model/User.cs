﻿using SqlSugar;
using System;

namespace EduCore.Models.Model
{
    /// <summary>
    /// 通过OAuth的获取到的用户信息（snsapi_userinfo=scope）
    /// </summary>
    public class User : RootEntity
    {
        public User()
        {
            CreateTime = DateTime.Now;
            UpdateTime = DateTime.Now;
        }
        ///// <summary>
        /// 用户的唯一标识
        /// </summary>
        [SugarColumn(Length = 32, IsNullable = false)]
        public string UserName { get; set; }

        /// <summary>
        /// 用户昵称
        /// </summary>
        [SugarColumn(Length = 64, IsNullable = false)]
        public string Password { get; set; }

        [SugarColumn(Length = 32, IsNullable = true)]
        public string NickName { get; set; }
        /// <summary>
        /// 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
        /// </summary>
        public int Sex { get; set; }

        /// <summary>
        /// 0为普通用户，1为管理员
        /// </summary>
        [SugarColumn(IsNullable = false)]
        public int Role { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public DateTime? UpdateTime { get; set; }


    }
}
