﻿using SqlSugar;
using System;

namespace EduCore.Models.Model
{
    /// <summary>
    /// 上课记录表
    /// </summary>
    public class StuSemRecord : RootEntity
    {
        public StuSemRecord()
        {
            CreateTime = DateTime.Now;
        }
        /// <summary>
        /// 学生Id
        /// </summary>
        [SugarColumn(IsNullable = false)]
        public int StuId { get; set; }
        /// <summary>
        /// 学期Id
        /// </summary>
        [SugarColumn(IsNullable = false)]
        public int SemId { get; set; }
        /// <summary>
        /// 当前课上课时间
        /// </summary>
        [SugarColumn(IsNullable = false)]
        public DateTime ClassStart { get; set; }
        /// <summary>
        /// 当前课下课时间
        /// </summary>
        [SugarColumn(IsNullable = false)]
        public DateTime ClassEnd { get; set; }
        /// <summary>
        /// 当前记录创建时间
        /// </summary>
        [SugarColumn(IsNullable = false)]
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 上课表现
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string Performance { get; set; }
        /// <summary>
        /// 学习内容
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string LearningContent { get; set; }
        /// <summary>
        /// 学生作业
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string Homework { get; set; }
    }
}
