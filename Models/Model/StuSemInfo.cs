﻿using SqlSugar;
using System;

namespace EduCore.Models.Model
{
    /// <summary>
    /// 学生学籍信息
    /// </summary>
    public class StuSemInfo : RootEntity
    {
        public StuSemInfo()
        {
            CreateTime = DateTime.Now;
        }
        /// <summary>
        /// 学生Id
        /// </summary>
        public int StuId { get; set; }

        /// <summary>
        /// 学期Id
        /// </summary>
        public int SemId { get; set; }
        /// <summary>
        /// 当期次数
        /// </summary>
        public int Times { get; set; }
        /// <summary>
        /// 当期剩余次数
        /// </summary>
        public int UsedTimes { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(Length = 100, IsNullable = true)]
        public string Remark { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

    }
}
