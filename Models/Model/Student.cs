﻿using SqlSugar;
using System;

namespace EduCore.Models.Model
{
    /// <summary>
    /// 学生信息表
    /// </summary>
    public class Student : RootEntity
    {
        public Student()
        {
            CreateTime = DateTime.Now;
        }
        ///// <summary>
        /// 学号
        /// </summary>
        [SugarColumn(Length = 32, IsNullable = false)]
        public string Stunum { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        [SugarColumn(Length = 8, IsNullable = false)]
        public string StuName { get; set; }
        /// <summary>
        /// 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
        /// </summary>
        public int Sex { get; set; }
        /// <summary>
        /// 学校
        /// </summary>
        [SugarColumn(Length = 60, IsNullable = true)]
        public string School { get; set; }

        /// <summary>
        /// 入学时间
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public DateTime? AdmissionDate { get; set; }

        /// <summary>
        /// 授权截至时间
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public DateTime? AuthDate { get; set; }

        /// <summary>
        /// 授权时间分钟
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public int AuthMin { get; set; }

        /// <summary>
        /// 入学年级
        /// </summary>
        [SugarColumn(Length = 12, IsNullable = true)]
        public string Grade { get; set; }

        [SugarColumn(Length = 100, IsNullable = true)]
        public string Remark { get; set; }

        public DateTime CreateTime { get; set; }

    }
}
