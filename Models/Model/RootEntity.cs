﻿using SqlSugar;
using System;

namespace EduCore.Models.Model
{
    public class RootEntity
    {
        /// <summary>
        /// ID
        /// </summary>
        [SugarColumn(IsNullable = false, IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; set; }


        /// <summary>
        /// ID
        /// </summary>
        [SugarColumn(IsNullable = false)]
        public bool IsDeleted { get; set; }


    }
}