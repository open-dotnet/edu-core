﻿using SqlSugar;
using System;

namespace EduCore.Models.Model
{
    /// <summary>
    /// 学期表 2019年二年级语文春季1班
    /// </summary>
    public class Semesters : RootEntity
    {
        public Semesters()
        {
            CreateTime = DateTime.Now;
        }

        /// <summary>
        /// 标题
        /// </summary>
        [SugarColumn(Length = 32, IsNullable = true)]
        public string Title { get; set; }

        /// <summary>
        /// 学年
        /// </summary>
        [SugarColumn(Length = 10, IsNullable = true)]
        public string Year { get; set; }

        /// <summary>
        /// 自定义学期名，如：春季
        /// </summary>
        [SugarColumn(Length = 10, IsNullable = true)]
        public string SemName { get; set; }

        /// <summary>
        /// 当期年级
        /// </summary>
        [SugarColumn(Length = 12, IsNullable = false)]
        public string Grade { get; set; }
        /// <summary>
        /// 学科名
        /// </summary>
        [SugarColumn(Length = 12, IsNullable = false)]
        public string Subject { get; set; }

        /// <summary>
        /// 班级数
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public int? ClassNum { get; set; }

        /// <summary>
        /// 班级学员数
        /// </summary>
        public int ClassSize { get; set; }


        /// <summary>
        /// 当前已报名学员数
        /// </summary>
        public int CurClassSize { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public DateTime? EndDate { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 学期备注
        /// </summary>        
        [SugarColumn(Length = 100, IsNullable = true)]
        public string Remark { get; set; }
    }
}
