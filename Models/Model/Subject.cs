﻿using SqlSugar;
using System;

namespace EduCore.Models.Model
{
    /// <summary>
    /// 年级信息表
    /// </summary>
    public class Subject : RootEntity
    {

        ///// <summary>
        /// 学号
        /// </summary>
        [SugarColumn(Length = 12, IsNullable = false)]
        public string Name { get; set; }
        [SugarColumn(IsNullable = false)]
        public int Sort { get; set; }

    }
}
