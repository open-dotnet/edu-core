﻿using SqlSugar;

namespace EduCore.Models.Model
{
    /// <summary>
    /// 通过OAuth的获取到的用户信息（snsapi_userinfo=scope）
    /// </summary>
    public class WechatStudent : RootEntity
    {
        ///// <summary>
        /// 用户的唯一标识
        /// </summary>
        public string Openid { get; set; }

        public int StuId { get; set; }

    }
}
