﻿using EduCore.IServices.BASE;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EduCore.IServices
{
    public interface IServiceWechatAuthUser : IBaseServices<WechatAuthUser>
    {
        Task<PageModel<WechatStudentView>> WechatStudentList(string openid, int intPageIndex = 1, int intPageSize = 20);
        Task<bool> ExistAsync(string openid);
        /// <summary>
        /// 根据用户昵称获取wechat用户
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        Task<List<WechatAuthUser>> WechatUser(string username);
    }
}
