﻿using EduCore.IServices.BASE;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EduCore.IServices
{
    /// <summary>
    /// 学生学籍信息
    /// </summary>
    public interface IServiceStuSemInfo : IBaseServices<StuSemInfo>
    {
        Task<bool> ExistAsync(StuSemInfo semesters);
        Task<int> UpdateSemestersClassAsync(StuSemInfo stuSemInfo);
        Task<int> ReduceSemestersClassAsync(StuSemInfo stuSemInfo);
        Task<PageModel<StuSemInfoView>> StuSemInfoViewList(string stuName, string stuNum, int semId, int intPageIndex = 1, int intPageSize = 20);
        Task<PageModel<StuSemInfoView>> StuSemInfo(int stuId, int intPageIndex = 1, int intPageSize = 20);

        Task<PageModel<StuInSemView>> StuInSemViewList(string query, int semid, int intPageIndex = 1, int intPageSize = 20);
    }
}
