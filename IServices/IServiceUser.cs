﻿using EduCore.IServices.BASE;
using EduCore.Models.Model;
using System.Threading.Tasks;

namespace EduCore.IServices
{
    /// <summary>
    /// 通过OAuth的获取到的用户信息（snsapi_userinfo=scope）
    /// </summary>
    public interface IServiceUser : IBaseServices<User>
    {
        Task<bool> ExistAsync(User semesters);
    }
}
