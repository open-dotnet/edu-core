﻿using EduCore.IServices.BASE;
using EduCore.Models.Model;
using System.Threading.Tasks;

namespace EduCore.IServices
{
    /// <summary>
    /// 学期表
    /// </summary>
    public interface IServiceSemesters : IBaseServices<Semesters>
    {

        Task<bool> ExistAsync(Semesters semesters);
        Task<int> UpdateSemestersClassAsync(Semesters semesters);
    }
}
