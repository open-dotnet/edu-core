﻿using EduCore.IServices.BASE;
using EduCore.Models.Model;
using System.Threading.Tasks;

namespace EduCore.IServices
{

    public interface IServiceGrade : IBaseServices<Grade>
    {
        Task<bool> ExistAsync(Grade grade, bool except = false);
    }
}
