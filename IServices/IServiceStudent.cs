﻿using EduCore.IServices.BASE;
using EduCore.Models.Model;
using System.Threading.Tasks;

namespace EduCore.IServices
{
    /// <summary>
    /// 学生信息表
    /// </summary>
    public interface IServiceStudent : IBaseServices<Student>
    {
        Task<bool> ExistAsync(Student semesters);
    }
}
