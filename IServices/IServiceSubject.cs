﻿using EduCore.IServices.BASE;
using EduCore.Models.Model;
using System.Threading.Tasks;

namespace EduCore.IServices
{

    public interface IServiceSubject : IBaseServices<Subject>
    {
        Task<bool> ExistAsync(Subject grade, bool except = false);
    }
}
