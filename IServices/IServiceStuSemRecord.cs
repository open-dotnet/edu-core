﻿using EduCore.IServices.BASE;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EduCore.IServices
{
    /// <summary>
    /// 上课记录表
    /// </summary>
    public interface IServiceStuSemRecord : IBaseServices<StuSemRecord>
    {
        Task<bool> ExistAsync(StuSemRecord semesters);
        Task<PageModel<StuSemRecordView>> StuSemInfoViewList(string stuName, string stuNum, int semId, int intPageIndex = 1, int intPageSize = 20);
        Task<PageModel<StuSemRecordView>> StuRecordList(int stuId, int semId, int intPageIndex = 1, int intPageSize = 20);
    }
}
