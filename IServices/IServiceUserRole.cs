﻿using EduCore.IServices.BASE;
using EduCore.Models.Model;

namespace EduCore.IServices
{
    /// <summary>
    /// 用户跟角色关联表
    /// </summary>
    public interface IServiceUserRole : IBaseServices<UserRole>
    {
    }
}
