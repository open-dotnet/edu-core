﻿using EduCore.IServices.BASE;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;
using System.Threading.Tasks;

namespace EduCore.IServices
{

    public interface IServiceWechatStudent : IBaseServices<WechatStudent>
    {
        Task<PageModel<WechatStudentInfoView>> WechatStudentInfoList(string nickname = null, string stuname = null, int intPageIndex = 1, int intPageSize = 20);
        Task<bool> ExistAsync(string openid, int stuId);
    }
}
