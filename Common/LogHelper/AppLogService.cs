﻿using log4net;

namespace EduCore.Common
{
    public class AppLogService : IAppLogService
    {
        private readonly ILog appInner = LogManager.GetLogger("Wechat", "AppInner");

        public void Info(object msg)
        {
            appInner.Info(msg);
        }

        public void Error(object msg)
        {
            appInner.Error(msg);
        }

        public void Warn(object msg)
        {
            appInner.Warn(msg);
        }

        public void Debug(object msg)
        {
            appInner.Debug(msg);
        }
    }
}
