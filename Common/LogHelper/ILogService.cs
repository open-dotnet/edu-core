namespace EduCore.IServices
{
    public interface ILogService
    {
        void Debug(object msg);
        void Error(object msg);
        void Info(object msg);
        void Warn(object msg);
    }
}