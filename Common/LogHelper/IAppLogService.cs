﻿namespace EduCore.Common
{
    public interface IAppLogService
    {
        void Debug(object msg);
        void Error(object msg);
        void Info(object msg);
        void Warn(object msg);
    }
}