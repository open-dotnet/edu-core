﻿using System;

namespace EduCore.Common
{
    public static class ViewHelper
    {
        public static string DateStringFromNow(DateTime dt)
        {
            var span = DateTime.UtcNow - dt;
            var min = 60;
            var hour = 3600;
            var day = 86400;
            var month = 2592000;
            var year = 31104000;

            var spansecond = span.TotalSeconds;
            if (Math.Abs(spansecond/year) >= 1)
            {
                return string.Format("{0}年前", (int) Math.Abs(spansecond/year));
            }
            if (Math.Abs(spansecond/month) >= 1)
            {
                return string.Format("{0}月前", (int) Math.Abs(spansecond/month));
            }
            if (Math.Abs(spansecond/day) >= 1)
            {
                return string.Format("{0}天前", (int) Math.Abs(spansecond/day));
            }
            if (Math.Abs(spansecond/hour) >= 1)
            {
                return string.Format("{0}小时前", (int) Math.Abs(spansecond/hour));
            }
            if (Math.Abs(spansecond/min) >= 1)
            {
                return string.Format("{0}分前", (int) Math.Abs(spansecond/min));
            }
            return "刚刚";
        }

        public static string DateString(DateTime dt)
        {
            dt = dt.AddHours(8);
            var now = DateTime.Now;
            if (dt.Year != now.Year)
            {
                return dt.ToString("yyyy-MM-dd");
            }
            if (dt.ToString("yyyy-MM-dd") ==now.ToString("yyyy-MM-dd"))
            {
                return "今天";
            }
            return dt.ToString("M月d日");
        }

        public static string TimeString(DateTime dt)
        {
            dt = dt.AddHours(8);
            return dt.ToString("HH:mm");
        }
    }
}