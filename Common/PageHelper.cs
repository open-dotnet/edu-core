﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace EduCore.Common
{
  public static class PageHelper
  {
    private static readonly string ImageHost = ConfigManager.ImageHost;

    public static string WithHttpImg(dynamic img)
    {
      string imghost = (string)img;
      if (string.IsNullOrEmpty(imghost))
      {
        imghost = "http://image.mzliaoba.com/pic/common/default_avatar.jpg";
      }
      else
      {

        if (imghost.IndexOf("http", StringComparison.Ordinal) != 0)
        {
          imghost = ImageHost + imghost;
        }
      }
      return imghost;
    }

    public static dynamic FormatNum(dynamic num)
    {
      try
      {
        double inum = (int)num;
        if (inum > 10000)
          return Math.Round(inum / 10000, 1) + "万";
        else return inum;
      }
      catch
      {
        return num;
      }
    }

    public static dynamic NumAdd(dynamic num, dynamic num1)
    {
      try
      {
        return (int)num + (int)num1;
      }
      catch
      {
        return num;
      }
    }
  }
}