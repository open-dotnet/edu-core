﻿using System;
using System.Linq;

namespace EduCore.Common
{
    public static class ConfigManager
    {
        public static readonly string DomainPath = AppDomain.CurrentDomain.BaseDirectory;
        /// <summary>
        /// API地址
        /// </summary>

        public static readonly string Host = Appsettings.app(new string[] { "AppSettings", "Server", "ApiHost" });
        public static readonly string CDN = Appsettings.app(new string[] { "AppSettings", "Server", "CDN" });
        public static readonly string MuzhiyunHost = Appsettings.app(new string[] { "AppSettings", "Server", "MuzhiyunHost" });
        public static readonly string ImageHost = Appsettings.app(new string[] { "AppSettings", "Server", "ImageHost" });
        public static readonly string ChatAppId = Appsettings.app(new string[] { "AppSettings", "Server", "ChatAppId" });
        public static readonly string AudioHost = Appsettings.app(new string[] { "AppSettings", "Server", "AudioHost" });
        public static readonly string ImageTemp = Appsettings.app(new string[] { "AppSettings", "Server", "ImageTemp" });
        public static readonly string AccessKeyId = Appsettings.app(new string[] { "AppSettings", "Server", "AccessKeyId" });
        public static readonly string AccessKeySecret = Appsettings.app(new string[] { "AppSettings", "Server", "AccessKeySecret" });


        public static readonly string TemplateId = Appsettings.app(new string[] { "SenparcWeixinSetting", "TemplateId" });
        public static readonly string WeixinRedirectUri = Appsettings.app(new string[] { "SenparcWeixinSetting", "WeixinRedirectUri" });
        public static readonly string AppId = Appsettings.app(new string[] { "SenparcWeixinSetting", "WeixinAppId" });
        public static readonly string WeixinAppSecret = Appsettings.app(new string[] { "SenparcWeixinSetting", "WeixinAppSecret" });


        //public static readonly string WeixinRedirectUri = GetAppConfigParamsValue("WeixinRedirectUri");


        //public static readonly string ImageTemp = GetAppConfigParamsValue("ImageTemp");
        //public static readonly string MuzhiyunHost = GetAppConfigParamsValue("MuzhiyunHost");

        //public static readonly string AppKey = GetAppConfigParamsValue("AppKey");
        //public static readonly string Channel = GetAppConfigParamsValue("Channel");
        //public static readonly string ClientVersion = GetAppConfigParamsValue("ClientVersion");

        //public static readonly string SockerAppid = GetAppConfigParamsValue("SockerAppid");
        //public static readonly string SockerAppsecret = GetAppConfigParamsValue("SockerAppsecret");

    }
}