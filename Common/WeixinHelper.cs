﻿using Senparc.Weixin.MP.Helpers;
using System.Configuration;

namespace InfoMedia.Core.Common
{
    public class WeixinHelper
    {
        /// <summary>
        ///     隐藏菜单列表
        /// </summary>
        public static string ShowMenuItems = "\"'menuItem:share:appMessage','menuItem:share:timeline'\"";

        private static readonly string _appId =  "WeixinAppId";
        private static readonly string _secret = "WeixinAppSecret";

        public static JsSdkUiPackage GetWxJsConfig(string url)
        {
#if DEBUG
            return new JsSdkUiPackage("","","","");
#else
            return JSSDKHelper.GetJsSdkUiPackage(_appId, _secret, url);
#endif
        }
    }

    
}