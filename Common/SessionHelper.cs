﻿using System;
using System.Web;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace EduCore.Common
{
    public static class SessionHelper
    {
        public static string GetSessionId()
        {
            return HttpContext.Current.Session.Id;
        }

        public static string GetSession(string key)
        {
            if (HttpContext.Current.Session != null)
            {
                var value = HttpContext.Current.Session.GetString(key);
                return value != null ? value.ToString() : string.Empty;
            }
            return string.Empty;
        }

        public static T GetSession<T>(string key)
        {
            string strvalue = GetSession(key);
            if (string.IsNullOrEmpty(strvalue))
            {
                return default(T);
            }
            else
            {
                return JsonConvert.DeserializeObject<T>(strvalue);
            }
        }


        public static void Clear()
        {
            HttpContext.Current.Session.Clear();
        }

        public static void DeleteSession(string key)
        {
            HttpContext.Current.Session.Remove(key);
        }

        public static string GetToken()
        {
            return GetSession("token");
        }

        public static void SetSession(string key, string value)
        {
            HttpContext.Current.Session.SetString(key, value);
        }

        public static void SetSession(string key, object value)
        {
            var strvalue = JsonConvert.SerializeObject(value);
            SetSession(key, strvalue);
        }
    }
}
