using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EduCore.IRepository;
using EduCore.Models.Model;
using EduCore.Models.ViewModel;
using EduCore.Repository.BASE;
using SqlSugar;
using SqlSugar.Extensions;

namespace EduCore.Repository
{
    /// <summary>
    /// UserRoleRepository
    /// </summary>	
    public class UserRoleRepository : BaseRepository<UserRole>, IRepositoryUserRole
    {


    }

    public class RepositorySemesters : BaseRepository<Semesters>, IRepositorySemesters
    {
        public async Task<int> UpdateSemestersClassAsync(Semesters semesters)
        {
            return await Db.Updateable(semesters)
                .UpdateColumns(sem => new { sem.CurClassSize })
                .ReSetValue(sem => sem.CurClassSize == sem.CurClassSize + 1).ExecuteCommandAsync();
        }

    }

    public class RepositoryWechatStudent : BaseRepository<WechatStudent>, IRepositoryWechatStudent
    {
        public async Task<PageModel<WechatStudentInfoView>> WechatStudentInfoList(string nickname = null, string stuname = null, int intPageIndex = 1, int intPageSize = 20)
        {
            RefAsync<int> totalCount = 0;
            var list = await Db.Queryable<WechatStudent, Student, WechatAuthUser>
                ((ws, stu, wa) => ws.StuId == stu.Id && ws.Openid == wa.Openid)
                .WhereIF(!string.IsNullOrEmpty(nickname), (ws, stu, wa) => wa.Nickname.Contains(nickname))
                .WhereIF(!string.IsNullOrEmpty(stuname), (ws, stu, wa) => stu.StuName.Contains(stuname))
                .OrderBy((ws) => ws.Id, OrderByType.Desc)
                .Select<WechatStudentInfoView>().ToPageListAsync(intPageIndex, intPageSize, totalCount);

            int pageCount = (Math.Ceiling(totalCount.ObjToDecimal() / intPageSize.ObjToDecimal())).ObjToInt();
            return new PageModel<WechatStudentInfoView>()
            {
                dataCount = totalCount,
                pageCount = pageCount,
                page = intPageIndex,
                PageSize = intPageSize,
                data = list
            };
        }

    }
    public class RepositoryWechatAuthUser : BaseRepository<WechatAuthUser>, IRepositoryWechatAuthUser
    {
        public async Task<PageModel<WechatStudentView>> WechatStudentList(string openid, int intPageIndex = 1, int intPageSize = 20)
        {
            RefAsync<int> totalCount = 0;
            var list = await Db.Queryable<WechatStudent, Student>
                ((ws, stu) => ws.Openid == openid && ws.StuId == stu.Id)
                .OrderBy((ws) => ws.Id, OrderByType.Desc)
                .Select<WechatStudentView>().ToPageListAsync(intPageIndex, intPageSize, totalCount);

            int pageCount = (Math.Ceiling(totalCount.ObjToDecimal() / intPageSize.ObjToDecimal())).ObjToInt();
            return new PageModel<WechatStudentView>()
            {
                dataCount = totalCount,
                pageCount = pageCount,
                page = intPageIndex,
                PageSize = intPageSize,
                data = list
            };
        }

    }
    public class RepositoryUserRole : BaseRepository<UserRole>, IRepositoryUserRole
    {


    }
    public class RepositoryUser : BaseRepository<User>, IRepositoryUser
    {


    }
    public class RepositoryStuSemRecord : BaseRepository<StuSemRecord>, IRepositoryStuSemRecord
    {
        public async Task<PageModel<StuSemRecordView>> StuRecordList(int stuId, int semId, int intPageIndex = 1, int intPageSize = 20)
        {
            RefAsync<int> totalCount = 0;
            var list = await Db.Queryable<StuSemRecord, Student, Semesters>
                ((ss, stu, sem) => ss.IsDeleted == false && ss.StuId == stu.Id && ss.SemId == sem.Id)
                .Where(ss => ss.StuId == stuId && ss.IsDeleted == false)
                .WhereIF(semId > 0, ss => ss.SemId == semId)
                .OrderBy((ss) => ss.Id, OrderByType.Desc)
                .Select<StuSemRecordView>().ToPageListAsync(intPageIndex, intPageSize, totalCount);

            int pageCount = (Math.Ceiling(totalCount.ObjToDecimal() / intPageSize.ObjToDecimal())).ObjToInt();
            return new PageModel<StuSemRecordView>()
            {
                dataCount = totalCount,
                pageCount = pageCount,
                page = intPageIndex,
                PageSize = intPageSize,
                data = list
            };
        }

        public async Task<PageModel<StuSemRecordView>> StuSemRecordViewList(string stuName, string stuNum, int semId, int intPageIndex = 1, int intPageSize = 20)
        {
            RefAsync<int> totalCount = 0;
            var list = await Db.Queryable<StuSemRecord, Student, Semesters>
                ((ss, stu, sem) => ss.IsDeleted == false && ss.StuId == stu.Id && ss.SemId == sem.Id)
                .OrderBy((ss) => ss.Id, OrderByType.Desc)
                .Where(ss => ss.IsDeleted == false)
                .WhereIF(!string.IsNullOrEmpty(stuName), (ss, stu, sem) => stu.StuName.Contains(stuName))
                .WhereIF(!string.IsNullOrEmpty(stuNum), (ss, stu, sem) => stu.Stunum.Contains(stuNum))
                .WhereIF(semId != 0, (ss, stu, sem) => ss.SemId == semId)
                .Select<StuSemRecordView>().ToPageListAsync(intPageIndex, intPageSize, totalCount);

            int pageCount = (Math.Ceiling(totalCount.ObjToDecimal() / intPageSize.ObjToDecimal())).ObjToInt();
            return new PageModel<StuSemRecordView>()
            {
                dataCount = totalCount,
                pageCount = pageCount,
                page = intPageIndex,
                PageSize = intPageSize,
                data = list
            };
        }
    }
    public class RepositoryStuSemInfo : BaseRepository<StuSemInfo>, IRepositoryStuSemInfo
    {
        public async Task<int> UpdateSemestersClassAsync(StuSemInfo stuSemInfo)
        {
            return await Db.Updateable(stuSemInfo)
                .UpdateColumns(sem => new { sem.UsedTimes })
                .Where(sem => sem.SemId == stuSemInfo.SemId && sem.StuId == stuSemInfo.StuId)
                .ReSetValue(sem => sem.UsedTimes == sem.UsedTimes + 1).ExecuteCommandAsync();
        }

        public async Task<int> ReduceSemestersClassAsync(StuSemInfo stuSemInfo)
        {
            return await Db.Updateable(stuSemInfo)
                .UpdateColumns(sem => new { sem.UsedTimes })
                .Where(sem => sem.SemId == stuSemInfo.SemId && sem.StuId == stuSemInfo.StuId)
                .ReSetValue(sem => sem.UsedTimes == sem.UsedTimes - 1).ExecuteCommandAsync();
        }
        public async Task<PageModel<StuInSemView>> StuInSemViewListAsync(string query, int semid, int intPageIndex = 1, int intPageSize = 20)
        {
            RefAsync<int> totalCount = 0;
            var list = await Db.Queryable<Student, StuSemInfo>
                ((stu, ss) => ss.StuId == stu.Id)
                .Where((stu, ss) => stu.IsDeleted == false && ss.IsDeleted == false && ss.SemId == semid)
                .WhereIF(!string.IsNullOrEmpty(query), stu => stu.StuName.Contains(query) || stu.Stunum.Contains(query))
                .OrderBy((stu) => stu.Id, OrderByType.Desc)
                .Select<StuInSemView>().ToPageListAsync(intPageIndex, intPageSize, totalCount);

            int pageCount = (Math.Ceiling(totalCount.ObjToDecimal() / intPageSize.ObjToDecimal())).ObjToInt();
            return new PageModel<StuInSemView>()
            {
                dataCount = totalCount,
                pageCount = pageCount,
                page = intPageIndex,
                PageSize = intPageSize,
                data = list
            };
        }

        public async Task<PageModel<StuSemInfoView>> StuSemInfoViewList(string stuName, string stuNum, int semId, int intPageIndex = 1, int intPageSize = 20)
        {
            RefAsync<int> totalCount = 0;
            var list = await Db.Queryable<StuSemInfo, Student, Semesters>
                ((ss, stu, sem) => ss.StuId == stu.Id && ss.SemId == sem.Id)
                .OrderBy(ss => ss.IsDeleted, OrderByType.Asc)
                .OrderBy(ss => ss.Id, OrderByType.Desc)
                .WhereIF(!string.IsNullOrEmpty(stuName), (ss, stu, sem) => stu.StuName.Contains(stuName))
                .WhereIF(!string.IsNullOrEmpty(stuNum), (ss, stu, sem) => stu.Stunum.Contains(stuNum))
                .WhereIF(semId != 0, (ss, stu, sem) => ss.SemId == semId)
                .Select<StuSemInfoView>().ToPageListAsync(intPageIndex, intPageSize, totalCount);

            int pageCount = (Math.Ceiling(totalCount.ObjToDecimal() / intPageSize.ObjToDecimal())).ObjToInt();
            return new PageModel<StuSemInfoView>()
            {
                dataCount = totalCount,
                pageCount = pageCount,
                page = intPageIndex,
                PageSize = intPageSize,
                data = list
            };
        }

        public async Task<PageModel<StuSemInfoView>> StuSemInfo(int stuId, int intPageIndex = 1, int intPageSize = 20)
        {
            RefAsync<int> totalCount = 0;
            var list = await Db.Queryable<StuSemInfo, Student, Semesters>
                ((ss, stu, sem) => ss.StuId == stu.Id && ss.SemId == sem.Id)
                .OrderBy(ss => ss.IsDeleted, OrderByType.Asc)
                .OrderBy(ss => ss.Id, OrderByType.Desc)
                .WhereIF(stuId != 0, (ss, stu, sem) => ss.StuId == stuId)
                .Select<StuSemInfoView>().ToPageListAsync(intPageIndex, intPageSize, totalCount);

            int pageCount = (Math.Ceiling(totalCount.ObjToDecimal() / intPageSize.ObjToDecimal())).ObjToInt();
            return new PageModel<StuSemInfoView>()
            {
                dataCount = totalCount,
                pageCount = pageCount,
                page = intPageIndex,
                PageSize = intPageSize,
                data = list
            };
        }
    }
    public class RepositoryStudent : BaseRepository<Student>, IRepositoryStudent
    {


    }

    public class RepositorySubject : BaseRepository<Subject>, IRepositorySubject
    {


    }
    public class RepositoryGrade : BaseRepository<Grade>, IRepositoryGrade
    {


    }
}
